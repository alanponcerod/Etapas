# Generated by Django 3.2.12 on 2022-06-03 01:28

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Admin',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('lastname', models.CharField(max_length=200)),
                ('created_at', models.DateTimeField()),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'db_table': 'admin',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Avatar',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('image', models.CharField(max_length=45)),
                ('default_color', models.CharField(max_length=100)),
                ('created_at', models.DateTimeField()),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'db_table': 'avatar',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(db_collation='utf8mb4_unicode_ci', max_length=45)),
                ('image', models.TextField(blank=True, db_collation='utf8mb4_unicode_ci', null=True)),
                ('color', models.CharField(blank=True, db_collation='utf8mb4_unicode_ci', max_length=45, null=True)),
                ('visible', models.IntegerField(blank=True, null=True)),
                ('editor', models.IntegerField()),
                ('created_at', models.DateTimeField()),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'db_table': 'category',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Course',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('admin_id', models.PositiveIntegerField(blank=True, null=True)),
                ('image', models.CharField(db_collation='utf8mb4_unicode_ci', max_length=100)),
                ('name', models.CharField(db_collation='utf8mb4_unicode_ci', max_length=200)),
                ('short_name', models.CharField(db_collation='utf8mb4_unicode_ci', max_length=45)),
                ('oss_project', models.CharField(db_collation='utf8mb4_unicode_ci', max_length=45)),
                ('created_at', models.DateTimeField()),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('version', models.IntegerField()),
                ('description', models.TextField(blank=True, db_collation='utf8mb4_unicode_ci', null=True)),
                ('summary', models.TextField(blank=True, db_collation='utf8mb4_unicode_ci', null=True)),
                ('visible', models.IntegerField()),
                ('duration_minutes', models.IntegerField()),
                ('start_date', models.DateTimeField(blank=True, null=True)),
                ('end_date', models.DateTimeField(blank=True, null=True)),
                ('problematic', models.TextField(blank=True, null=True)),
                ('challenge_number', models.IntegerField(blank=True, null=True)),
                ('delivery_type', models.CharField(blank=True, max_length=255, null=True)),
                ('status', models.CharField(blank=True, max_length=10, null=True)),
                ('relevance_level', models.IntegerField()),
                ('verified_competences', models.IntegerField()),
            ],
            options={
                'db_table': 'course',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='CourseType',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(db_collation='utf8mb4_unicode_ci', max_length=45)),
                ('created_at', models.DateTimeField()),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'db_table': 'course_type',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Enterprise',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type', models.CharField(blank=True, max_length=30, null=True)),
                ('domain', models.CharField(blank=True, max_length=200, null=True)),
                ('max_talents', models.IntegerField()),
                ('address', models.CharField(blank=True, max_length=150, null=True)),
                ('state', models.CharField(blank=True, max_length=45, null=True)),
                ('city', models.CharField(blank=True, max_length=45, null=True)),
                ('country', models.CharField(blank=True, max_length=45, null=True)),
                ('website', models.CharField(blank=True, max_length=100, null=True)),
                ('employees', models.CharField(blank=True, max_length=20, null=True)),
                ('emp_six_month', models.IntegerField(blank=True, null=True)),
                ('summary', models.TextField(blank=True, null=True)),
                ('information', models.TextField(blank=True, null=True)),
                ('acknowledgments', models.TextField(blank=True, null=True)),
                ('benefits', models.TextField(blank=True, null=True)),
                ('deleted_at', models.DateTimeField(blank=True, null=True)),
                ('created_at', models.DateTimeField(blank=True, null=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('area_id', models.IntegerField(blank=True, null=True)),
                ('personal_name', models.CharField(blank=True, max_length=255, null=True)),
                ('lastname', models.CharField(blank=True, max_length=255, null=True)),
                ('occupation', models.CharField(blank=True, max_length=255, null=True)),
                ('wepowsubdomain', models.CharField(blank=True, db_column='wepowSubdomain', max_length=255, null=True)),
                ('stripe_customer', models.CharField(blank=True, max_length=100, null=True)),
                ('udemy_project_id', models.PositiveIntegerField(blank=True, null=True)),
                ('administrative_contact', models.TextField(blank=True, null=True)),
                ('enable_extra_points', models.IntegerField()),
                ('finished_project', models.IntegerField()),
                ('phone', models.CharField(blank=True, max_length=60, null=True)),
                ('institution_img', models.CharField(blank=True, max_length=255, null=True)),
                ('enable_rhh', models.IntegerField()),
                ('enable_coordinators_udemy', models.IntegerField()),
                ('enabled', models.IntegerField()),
                ('is_qualifinds', models.IntegerField()),
                ('is_upland', models.IntegerField()),
                ('istalentpool', models.IntegerField(db_column='isTalentPool')),
                ('is_talent_pool_rh', models.IntegerField()),
            ],
            options={
                'db_table': 'enterprise',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Evaluation',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(db_collation='utf8mb4_unicode_ci', max_length=160)),
                ('description', models.TextField(blank=True, db_collation='utf8mb4_unicode_ci', null=True)),
                ('instructions', models.TextField(blank=True, db_collation='utf8mb4_unicode_ci', null=True)),
                ('start_date', models.DateTimeField(blank=True, null=True)),
                ('end_date', models.DateTimeField(blank=True, null=True)),
                ('limit_time', models.IntegerField()),
                ('min_score', models.FloatField()),
                ('weight', models.IntegerField(blank=True, null=True)),
                ('max_intents', models.IntegerField()),
                ('random', models.IntegerField()),
                ('random_answers', models.IntegerField()),
                ('points', models.IntegerField(blank=True, null=True)),
                ('created_at', models.DateTimeField()),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('duration_minutes', models.IntegerField()),
                ('smart', models.IntegerField()),
                ('smart_level', models.IntegerField()),
                ('status', models.CharField(max_length=10)),
                ('micro', models.IntegerField()),
                ('max_micro', models.IntegerField()),
                ('display_type', models.CharField(blank=True, max_length=50, null=True)),
                ('relevance_level', models.IntegerField()),
                ('evaluation_id_reference', models.PositiveIntegerField(blank=True, null=True)),
            ],
            options={
                'db_table': 'evaluation',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='EvaluationType',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=45)),
                ('prefix_url', models.CharField(max_length=3)),
                ('variant', models.CharField(blank=True, max_length=5, null=True)),
                ('created_at', models.DateTimeField()),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'db_table': 'evaluation_type',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Industry',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=45)),
                ('name_en', models.CharField(max_length=45)),
                ('created_at', models.DateTimeField()),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'db_table': 'industry',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Path',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('admin_id', models.PositiveIntegerField(blank=True, null=True)),
                ('created_at', models.DateTimeField()),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'db_table': 'path',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='PathStep',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number', models.IntegerField()),
                ('created_at', models.DateTimeField()),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'db_table': 'path_step',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='StepElement',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('order', models.IntegerField()),
                ('created_at', models.DateTimeField()),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
            ],
            options={
                'db_table': 'step_element',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='Users',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=191)),
                ('email', models.CharField(max_length=191, unique=True)),
                ('password', models.CharField(max_length=191)),
                ('type_user', models.IntegerField(blank=True, null=True)),
                ('remember_token', models.CharField(blank=True, max_length=100, null=True)),
                ('created_at', models.DateTimeField(blank=True, null=True)),
                ('updated_at', models.DateTimeField(blank=True, null=True)),
                ('img', models.CharField(blank=True, max_length=255, null=True)),
                ('facebook_id', models.CharField(blank=True, max_length=255, null=True)),
                ('linkedin_id', models.CharField(blank=True, max_length=255, null=True)),
                ('verify', models.IntegerField()),
                ('verify_token', models.CharField(blank=True, max_length=255, null=True)),
                ('recover_password', models.CharField(blank=True, max_length=255, null=True)),
                ('wepowapikey', models.CharField(blank=True, db_column='wepowApikey', max_length=255, null=True)),
                ('temporal_password', models.IntegerField()),
                ('terms_privacy', models.IntegerField()),
                ('deleted_at', models.DateTimeField(blank=True, null=True)),
                ('lang', models.CharField(max_length=45)),
                ('default_scope', models.CharField(blank=True, max_length=45, null=True)),
                ('default_profile_id', models.PositiveIntegerField(blank=True, null=True)),
            ],
            options={
                'db_table': 'users',
                'managed': False,
            },
        ),
    ]
