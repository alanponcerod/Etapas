from django.urls import path
from . import views
from django.contrib.auth import views as auth_views

app_name = 'etapasApp'

urlpatterns = [
	path('', views.IndexView, name =  "index"),

	path('trayectoria/', views.TrayectoriaView, name='trayectoria' ),
	path('trayectoria/<int:curso_id>', views.TrayectoriaView, name =  "trayectoria_id"),
	path('actualizarStepElement/<int:curso_id>/<int:step_id>/', views.ActualizarStepElement.as_view(), name='actualizar'),
	path('carga_trayectoria/', views.CargaTrayectoriaView, name='carga-trayectoria'),
	]