from django.contrib import admin
from .models import *
from import_export import resources
from import_export.admin import ImportExportModelAdmin
# Register your models here.


class UsersResource(resources.ModelResource):
	class Meta:
		model = Users

class UsersAdminC(ImportExportModelAdmin):
	form_encoding = "utf-8"
	resource_class = UsersResource

admin.site.register(Users, UsersAdminC)

