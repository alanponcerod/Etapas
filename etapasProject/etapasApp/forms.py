from django import forms
from django.db.models import Q
import datetime
from .models import *

class StepElementForm(forms.ModelForm):
	class Meta:
		model=StepElement
		fields = (
        'path_step',
        'order'
        )
		widgets = {					
			'path_step':forms.TextInput(attrs={'class': 'form-control'}),
			'order':forms.TextInput(attrs={'class': 'form-control'})			
			}

class ReaderForm(forms.ModelForm):
    class Meta:
        model = Reader
        fields = ['file', 'file2']