from django.shortcuts import render, reverse, HttpResponseRedirect
from .models import *
from .forms import *
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.shortcuts import render, redirect, get_object_or_404
from django.core.exceptions import MultipleObjectsReturned
import chardet
import pandas as pd
import os
import datetime
from datetime import date

# Create your views here.
def IndexView(request):
	cursos_guardados = Course.objects.all()
	return render (request, 'etapasApp/index.html', {'cursos_guardados': cursos_guardados})


def TrayectoriaView(request, curso_id):

	cursos_guardados = Course.objects.all()

	curso_1 = Course.objects.get(id=curso_id)
	print("curso_1: ", curso_1.name)

	try:
		path_id_del_curso = Path.objects.filter(course=curso_1.id)
		#print("path_id_del_curso.id: ", path_id_del_curso.id)
	except (Path.DoesNotExist, MultipleObjectsReturned) as e:
		return redirect('etapasApp:index')
	paths = Path.objects.all()
	path_steps = PathStep.objects.all()
	step_elements = StepElement.objects.all()
	evaluations = Evaluation.objects.all()
	courses = Course.objects.all()
	enterprises = Enterprise.objects.all()
	competences = Competence.objects.all()
	evaluation_competences = EvaluationCompetence.objects.all()
	course_competences = CourseCompetence.objects.all()
	path_step_y_number =[]


	for indx, pid in enumerate(path_id_del_curso):
		path_step_ids_del_path = PathStep.objects.filter(path = pid.id)
		print("path_step_ids_del_path.ids: ")

		

		

		for index, path_step_id in enumerate(path_step_ids_del_path):
			path_step_y_number.append([path_step_id.id, path_step_id.number])
			print("renglon numero : "+str(index)+" id = " +str(path_step_id.id)+ " num = "+str(path_step_id.number))
			step_element = StepElement.objects.filter(path_step = path_step_id.id)
			for index2, step_ele in enumerate(step_element):
				try:
					evaluation = Evaluation.objects.get(id = step_ele.evaluation_id )
					print("evaluacion = "+str(evaluation.name))
				except Evaluation.DoesNotExist:
					course = Course.objects.get(id = step_ele.course_id)
					print("course = "+str(course.name))
	return render (request, 'etapasApp/trayectoria.html', {'cursos_guardados': cursos_guardados, 'curso_1':curso_1, \
		'path_id_del_curso':path_id_del_curso , 'path_steps':path_steps , \
		'step_elements':step_elements, 'evaluations': evaluations, 'path_step_y_number':path_step_y_number, 'courses': courses,\
		'enterprises': enterprises, 'competences': competences, 'paths': paths, 'evaluation_competences': evaluation_competences,\
		'course_competences': course_competences})

def CargaTrayectoriaView(request):
    lista_verificacion = []
    lista_verificacion_BD = []
    valido = True 
    valido2 = True
    if request.method == 'POST':
        form = ReaderForm(request.POST or None, request.FILES or None)
        if form.is_valid():
            

            #--------------preguntas del archivo nuevo a cargar
            file = form.cleaned_data.get('file')# archivo preguntas
            file2 = form.cleaned_data.get('file2')# archivo config
            obj = form.save(commit=False)
            #print("type(file): ", type(file))
            obj.file = file
            obj.file2 = file2
            obj.save()

            #f = obj.file.open('r')
            #reader_file = csv.reader(open(obj.file.path,'r'))
            with open(obj.file.path, 'rb') as f:
                result = chardet.detect(f.read())  # or readline if the file is large
            with open(obj.file2.path, 'rb') as f2:
                result2 = chardet.detect(f2.read())  # or readline if the file is large

            #pd.read_csv('filename.csv', )
            df = pd.read_csv(open(obj.file.path,'rb'), encoding=result['encoding'])#archivo de preguntas
            dffile2 = pd.read_csv(open(obj.file2.path,'rb'), encoding=result2['encoding'])# archivo de config
            print("type(df.columns): ", type(df.columns))
            print("dffile2.columns: ", dffile2.columns.all)
            columnas_config = ['Nombre Diagnostico', 'Competence ID', 'Competence', 'Name Perfil', 'Description', 'Instructions', 'Min score', 'Default attempts',\
            'Weight', 'Evaluation rule', 'Method Rule', 'Evaluation type', 'Diagnostic points']
            columnas_preg = ['ID Pregunta', 'Código', 'ID Competencia', 'Competencia','Prerrequisitos', 'Módulo', 'Tipo de Evaluación', 'Concepto',\
            'Tipo de ejercicio', 'Puntaje', 'Nivel', 'Instrucción', 'Descripción', 'Respuesta', 'Ponderación', 'Respuesta.1', 'Ponderación.1',\
            'Respuesta.2', 'Ponderación.2', 'Respuesta.3', 'Ponderación.3', 'Feedback', 'Recurso - Nombre', 'Recurso - URL', 'Idioma', 'Recurso - Nombre.1', \
            'Recurso - URL.1', 'Idioma.1', 'Recurso - Nombre.2', 'Recurso - URL.2', 'Idioma.2', 'Instrucciones del módulo']
            if columnas_config == dffile2.columns.tolist():
                print("columnas de config iguales")
            else:
                valido = False
                open(obj.file.path,'r').close()
                open(obj.file2.path,'r').close()
                os.remove(obj.file.path)
                os.remove(obj.file2.path)
                return render(request, 'etapasApp/carga_trayectoria.html',{'form': form, 'valido': valido, 'valido2': valido2, 'lista_verificacion' : lista_verificacion, 'lista_verificacion_BD' : lista_verificacion_BD})

            if columnas_preg == df.columns.tolist():
                print("columnas de preguntas iguales")
            else:
                valido2 = False
                open(obj.file.path,'r').close()
                open(obj.file2.path,'r').close()
                os.remove(obj.file.path)
                os.remove(obj.file2.path)
                return render(request, 'etapasApp/carga_trayectoria.html',{'form': form, 'valido': valido, 'valido2': valido2, 'lista_verificacion' : lista_verificacion, 'lista_verificacion_BD' : lista_verificacion_BD})

            #------EMPIEZA LECTURA DEL ARCHIVO DE CONFIG

            #------------------CARGA DE TABLA COURSE DEL ARCHIVO DE CONFIG-----------
            course_name = dffile2.iloc[0][3]
            existe_course = Course.objects.filter(name=course_name).exists()
            print("existe_course", existe_course)

            if not existe_course:                
                course_name = dffile2.iloc[0][3]
                course_short_name_list = [ s[0]+s[1]+s[2] if len(s) >= 3 else s[0] for s in course_name.split() ]
                course_short_name = ''.join(course_short_name_list).upper()
                course_created_at = datetime.datetime.now()
                course_updated_at = datetime.datetime.now()

            #------TERMINA LECTURA DEL ARCHIVO DE CONFIG
            open(obj.file.path,'r').close()
            open(obj.file2.path,'r').close()
            os.remove(obj.file.path)
            os.remove(obj.file2.path)
            return render(request, 'etapasApp/carga_trayectoria.html',{'form': form, 'valido': valido, 'valido2': valido2, 'lista_verificacion' : lista_verificacion, 'lista_verificacion_BD' : lista_verificacion_BD})
            
            



    else:
        form = ReaderForm()
        return render(request, 'etapasApp/carga_trayectoria.html',{'form': form, 'valido': valido, 'valido2': valido2, 'lista_verificacion' : lista_verificacion, 'lista_verificacion_BD' : lista_verificacion_BD})

class ActualizarStepElement(UpdateView):
    model = StepElement
    form_class = StepElementForm
    template_name = 'etapasApp/modM.html'
    #success_url = reverse_lazy('etapasApp:trayectoria')

    def get_success_url(self):
          # if you are passing 'pk' from 'urls' to 'DeleteView' for company
          # capture that 'pk' as companyid and pass it to 'reverse_lazy()' function
         cursoid=self.kwargs['curso_id']
         print("cursoid:",cursoid)
         return reverse('etapasApp:trayectoria_id', kwargs={"curso_id": self.kwargs['curso_id']})

    def get_object(self):
    	print("dfd", self.request.POST.get('pk'))
    	obj = get_object_or_404(StepElement, id=self.kwargs['step_id'])
    	print(obj)
    	return obj

    def get_context_data(self, **kwargs):
        context = super(ActualizarStepElement, self).get_context_data(**kwargs)
        context['curso_1'] = Course.objects.get(id=self.kwargs['curso_id']) #whatever you would like
        return context

    def form_valid(self, form):
        form.instance.save()
        return super().form_valid(form)