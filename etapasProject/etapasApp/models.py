# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models
from django.utils import timezone

# python manage.py inspectdb course > output.py
#-------------------------empieza definicion de tablas para que funcione course----------

class Reader(models.Model):
    file = models.FileField(blank=True, null=True)
    file2 = models.FileField(blank=True, null=True)
    date_uploaded = models.DateTimeField(default=timezone.now)

    class Meta:
        ordering = ['-date_uploaded']
        verbose_name_plural = "Reader"

class Course(models.Model):
    admin_id = models.PositiveIntegerField(blank=True, null=True)
    image = models.CharField(max_length=100, db_collation='utf8mb4_unicode_ci')
    name = models.CharField(max_length=200, db_collation='utf8mb4_unicode_ci')
    short_name = models.CharField(max_length=45, db_collation='utf8mb4_unicode_ci')
    oss_project = models.CharField(max_length=45, db_collation='utf8mb4_unicode_ci')
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)
    version = models.IntegerField()
    description = models.TextField(db_collation='utf8mb4_unicode_ci', blank=True, null=True)
    summary = models.TextField(db_collation='utf8mb4_unicode_ci', blank=True, null=True)
    visible = models.IntegerField()
    course_type = models.ForeignKey('CourseType', models.DO_NOTHING)
    category = models.ForeignKey('Category', models.DO_NOTHING)
    duration_minutes = models.IntegerField()
    start_date = models.DateTimeField(blank=True, null=True)
    end_date = models.DateTimeField(blank=True, null=True)
    problematic = models.TextField(blank=True, null=True)
    challenge_number = models.IntegerField(blank=True, null=True)
    delivery_type = models.CharField(max_length=255, blank=True, null=True)
    enterprise = models.ForeignKey('Enterprise', models.DO_NOTHING, blank=True, null=True)
    status = models.CharField(max_length=10, blank=True, null=True)
    avatar = models.ForeignKey('Avatar', models.DO_NOTHING)
    relevance_level = models.IntegerField()
    verified_competences = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'course'


class CourseType(models.Model):
    name = models.CharField(max_length=45, db_collation='utf8mb4_unicode_ci')
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'course_type'


class Category(models.Model):
    name = models.CharField(max_length=45, db_collation='utf8mb4_unicode_ci')
    image = models.TextField(db_collation='utf8mb4_unicode_ci', blank=True, null=True)
    color = models.CharField(max_length=45, db_collation='utf8mb4_unicode_ci', blank=True, null=True)
    visible = models.IntegerField(blank=True, null=True)
    editor = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'category'


class Enterprise(models.Model):
    user = models.ForeignKey('Users', models.DO_NOTHING)
    type = models.CharField(max_length=30, blank=True, null=True)
    domain = models.CharField(max_length=200, blank=True, null=True)
    max_talents = models.IntegerField()
    address = models.CharField(max_length=150, blank=True, null=True)
    state = models.CharField(max_length=45, blank=True, null=True)
    city = models.CharField(max_length=45, blank=True, null=True)
    country = models.CharField(max_length=45, blank=True, null=True)
    website = models.CharField(max_length=100, blank=True, null=True)
    employees = models.CharField(max_length=20, blank=True, null=True)
    emp_six_month = models.IntegerField(blank=True, null=True)
    summary = models.TextField(blank=True, null=True)
    information = models.TextField(blank=True, null=True)
    acknowledgments = models.TextField(blank=True, null=True)
    benefits = models.TextField(blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    area_id = models.IntegerField(blank=True, null=True)
    personal_name = models.CharField(max_length=255, blank=True, null=True)
    lastname = models.CharField(max_length=255, blank=True, null=True)
    occupation = models.CharField(max_length=255, blank=True, null=True)
    wepowsubdomain = models.CharField(db_column='wepowSubdomain', max_length=255, blank=True, null=True)  # Field name made lowercase.
    stripe_customer = models.CharField(max_length=100, blank=True, null=True)
    udemy_project_id = models.PositiveIntegerField(blank=True, null=True)
    administrative_contact = models.TextField(blank=True, null=True)
    enable_extra_points = models.IntegerField()
    finished_project = models.IntegerField()
    industry = models.ForeignKey('Industry', models.DO_NOTHING, blank=True, null=True)
    phone = models.CharField(max_length=60, blank=True, null=True)
    institution_img = models.CharField(max_length=255, blank=True, null=True)
    enable_rhh = models.IntegerField()
    enable_coordinators_udemy = models.IntegerField()
    enabled = models.IntegerField()
    is_qualifinds = models.IntegerField()
    is_upland = models.IntegerField()
    istalentpool = models.IntegerField(db_column='isTalentPool')  # Field name made lowercase.
    is_talent_pool_rh = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'enterprise'


class Avatar(models.Model):
    name = models.CharField(max_length=50)
    image = models.CharField(max_length=45)
    default_color = models.CharField(max_length=100)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'avatar'

class Industry(models.Model):
    name = models.CharField(max_length=45)
    name_en = models.CharField(max_length=45)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'industry'


class Users(models.Model):
    name = models.CharField(max_length=191)
    email = models.CharField(unique=True, max_length=191)
    password = models.CharField(max_length=191)
    type_user = models.IntegerField(blank=True, null=True)
    remember_token = models.CharField(max_length=100, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    img = models.CharField(max_length=255, blank=True, null=True)
    facebook_id = models.CharField(max_length=255, blank=True, null=True)
    linkedin_id = models.CharField(max_length=255, blank=True, null=True)
    verify = models.IntegerField()
    verify_token = models.CharField(max_length=255, blank=True, null=True)
    recover_password = models.CharField(max_length=255, blank=True, null=True)
    wepowapikey = models.CharField(db_column='wepowApikey', max_length=255, blank=True, null=True)  # Field name made lowercase.
    temporal_password = models.IntegerField()
    terms_privacy = models.IntegerField()
    deleted_at = models.DateTimeField(blank=True, null=True)
    lang = models.CharField(max_length=45)
    default_scope = models.CharField(max_length=45, blank=True, null=True)
    default_profile_id = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'users'

#-------------------------termina tabals para que funcione course----------

#-------------------------empieza definicion de tablas para que funcione evaluation----------

class Evaluation(models.Model):
    admin = models.ForeignKey('Admin', models.DO_NOTHING, blank=True, null=True)
    enterprise = models.ForeignKey('Enterprise', models.DO_NOTHING, blank=True, null=True)
    name = models.CharField(max_length=160, db_collation='utf8mb4_unicode_ci')
    description = models.TextField(db_collation='utf8mb4_unicode_ci', blank=True, null=True)
    instructions = models.TextField(db_collation='utf8mb4_unicode_ci', blank=True, null=True)
    start_date = models.DateTimeField(blank=True, null=True)
    end_date = models.DateTimeField(blank=True, null=True)
    limit_time = models.IntegerField()
    min_score = models.FloatField()
    weight = models.IntegerField(blank=True, null=True)
    max_intents = models.IntegerField()
    random = models.IntegerField()
    random_answers = models.IntegerField()
    points = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)
    duration_minutes = models.IntegerField()
    evaluation_type = models.ForeignKey('EvaluationType', models.DO_NOTHING)
    smart = models.IntegerField()
    smart_level = models.IntegerField()
    status = models.CharField(max_length=10)
    micro = models.IntegerField()
    max_micro = models.IntegerField()
    display_type = models.CharField(max_length=50, blank=True, null=True)
    relevance_level = models.IntegerField()
    evaluation_id_reference = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'evaluation'

class Admin(models.Model):
    lastname = models.CharField(max_length=200)
    user = models.ForeignKey('Users', models.DO_NOTHING, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'admin'

class EvaluationType(models.Model):
    name = models.CharField(max_length=45)
    prefix_url = models.CharField(max_length=3)
    variant = models.CharField(max_length=5, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'evaluation_type'
#-------------------------termina tabals para que funcione evaluation----------

#-------------------------empieza definicion de tablas para que funcione path----------
class Path(models.Model):
    course = models.ForeignKey('Course', models.DO_NOTHING)
    enterprise = models.ForeignKey('Enterprise', models.DO_NOTHING, blank=True, null=True)
    admin_id = models.PositiveIntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'path'

#-------------------------termina tabals para que funcione path----------
#-------------------------empieza definicion de tablas para que funcione path_step----------
class PathStep(models.Model):
    path = models.ForeignKey('Path', models.DO_NOTHING)
    number = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'path_step'

#-------------------------termina tabals para que funcione path_step----------

#-------------------------empieza definicion de tablas para que funcione step_element----------
class StepElement(models.Model):
    path_step = models.ForeignKey('PathStep', models.DO_NOTHING)
    course = models.ForeignKey('Course', models.DO_NOTHING, blank=True, null=True)
    evaluation = models.ForeignKey('Evaluation', models.DO_NOTHING, blank=True, null=True)
    order = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'step_element'

#-------------------------termina tabals para que funcione path_step----------


#-------------------------empieza definicion de tablas para que funcione competence----------
class Competence(models.Model):
    admin = models.ForeignKey('Admin', models.DO_NOTHING, blank=True, null=True)
    enterprise = models.ForeignKey('Enterprise', models.DO_NOTHING, blank=True, null=True)
    name = models.CharField(max_length=160, db_collation='utf8mb4_unicode_ci')
    name_en = models.CharField(max_length=160, blank=True, null=True)
    description = models.TextField(db_collation='utf8mb4_unicode_ci', blank=True, null=True)
    description_en = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)
    shortname = models.CharField(max_length=100, db_collation='utf8mb4_unicode_ci', blank=True, null=True)
    release = models.CharField(max_length=255, blank=True, null=True)
    status = models.CharField(max_length=10)

    class Meta:
        managed = False
        db_table = 'competence'
#-------------------------termina tabals para que funcione path_step----------

#-------------------------empieza definicion de tablas para que funcione evaluation_competence----------
class EvaluationCompetence(models.Model):
    competence = models.ForeignKey('Competence', models.DO_NOTHING)
    evaluation = models.ForeignKey('Evaluation', models.DO_NOTHING)
    weight = models.FloatField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'evaluation_competence'

#-------------------------termina tabals para que funcione evaluation_competence----------

#-------------------------empieza definicion de tablas para que funcione course_competence----------
class CourseCompetence(models.Model):
    competence = models.ForeignKey('Competence', models.DO_NOTHING)
    course = models.ForeignKey('Course', models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)
    weight = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'course_competence'

#-------------------------termina tabals para que funcione course_competence----------