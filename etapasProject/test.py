# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class CursosCourseretiremodel(models.Model):
    creado_en = models.DateTimeField(blank=True, null=True)
    actualizado_en = models.DateTimeField(blank=True, null=True)
    id_course = models.IntegerField(unique=True)
    scheduled_removal_date = models.DateTimeField()
    language = models.CharField(max_length=2000)
    title = models.CharField(max_length=2000, blank=True, null=True)
    course_category = models.CharField(max_length=2000)
    course_subcategory = models.CharField(max_length=2000)
    alternative_course_1 = models.IntegerField()
    title_alternative_course_1 = models.CharField(max_length=2000)
    alternative_course_2 = models.IntegerField()
    title_alternative_course_2 = models.CharField(max_length=2000)

    class Meta:
        managed = False
        db_table = 'Cursos_courseretiremodel'


class ActionMethod(models.Model):
    description = models.TextField()
    action = models.CharField(max_length=45)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'action_method'


class ActionRule(models.Model):
    action = models.CharField(max_length=45)
    description = models.TextField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'action_rule'


class Admin(models.Model):
    lastname = models.CharField(max_length=200)
    user = models.ForeignKey('Users', models.DO_NOTHING, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'admin'


class Answer(models.Model):
    question = models.ForeignKey('Question', models.DO_NOTHING)
    answer = models.TextField(db_collation='utf8mb4_unicode_ci')
    qualification = models.FloatField()
    order = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'answer'


class AnswerFeedback(models.Model):
    feedback = models.ForeignKey('Feedback', models.DO_NOTHING)
    answer = models.ForeignKey(Answer, models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'answer_feedback'


class Aspect(models.Model):
    rubric = models.ForeignKey('Rubric', models.DO_NOTHING)
    description = models.TextField()
    description_en = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'aspect'


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.IntegerField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class AvailableFor(models.Model):
    course = models.ForeignKey('Course', models.DO_NOTHING)
    enterprise = models.ForeignKey('Enterprise', models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'available_for'


class Avatar(models.Model):
    name = models.CharField(max_length=50)
    image = models.CharField(max_length=45)
    default_color = models.CharField(max_length=100)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'avatar'


class BackupJsonInternship(models.Model):
    user = models.ForeignKey('Users', models.DO_NOTHING)
    enterprise = models.ForeignKey('Enterprise', models.DO_NOTHING, blank=True, null=True)
    admin = models.ForeignKey(Admin, models.DO_NOTHING, blank=True, null=True)
    course_id = models.PositiveIntegerField(blank=True, null=True)
    name = models.CharField(max_length=200, blank=True, null=True)
    json = models.TextField(blank=True, null=True)
    key = models.CharField(max_length=100)
    action = models.CharField(max_length=12)
    status = models.CharField(max_length=10)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'backup_json_internship'


class Candidate(models.Model):
    talent = models.ForeignKey('Talent', models.DO_NOTHING)
    enterprise = models.ForeignKey('Enterprise', models.DO_NOTHING)
    status = models.CharField(max_length=9)
    confirmation_code = models.CharField(max_length=100, blank=True, null=True)
    reason = models.TextField(blank=True, null=True)
    executive = models.ForeignKey('Executive', models.DO_NOTHING, blank=True, null=True)
    evaluator = models.ForeignKey('Evaluator', models.DO_NOTHING, blank=True, null=True)
    job = models.ForeignKey('Job', models.DO_NOTHING, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'candidate'


class Candidates(models.Model):
    talent_id = models.IntegerField()
    enterprise_id = models.IntegerField()
    step = models.IntegerField()
    status = models.IntegerField()
    discard = models.IntegerField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'candidates'


class Category(models.Model):
    name = models.CharField(max_length=45, db_collation='utf8mb4_unicode_ci')
    image = models.TextField(db_collation='utf8mb4_unicode_ci', blank=True, null=True)
    color = models.CharField(max_length=45, db_collation='utf8mb4_unicode_ci', blank=True, null=True)
    visible = models.IntegerField(blank=True, null=True)
    editor = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'category'


class Challenge(models.Model):
    admin = models.ForeignKey(Admin, models.DO_NOTHING, blank=True, null=True)
    name = models.TextField(db_collation='utf8mb4_unicode_ci')
    description = models.TextField(db_collation='utf8mb4_unicode_ci')
    instructions = models.TextField(db_collation='utf8mb4_unicode_ci', blank=True, null=True)
    position = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)
    university = models.ForeignKey('Universities', models.DO_NOTHING, blank=True, null=True)
    enterprise = models.ForeignKey('Enterprise', models.DO_NOTHING, blank=True, null=True)
    duration_minutes = models.IntegerField(blank=True, null=True)
    competence = models.ForeignKey('Competence', models.DO_NOTHING)
    shortname = models.CharField(max_length=255, blank=True, null=True)
    estimated_days = models.IntegerField()
    challenge_id_reference = models.PositiveIntegerField(blank=True, null=True)
    status = models.CharField(max_length=10)

    class Meta:
        managed = False
        db_table = 'challenge'


class ChallengeCompetence(models.Model):
    competence = models.ForeignKey('Competence', models.DO_NOTHING)
    challenge = models.ForeignKey(Challenge, models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)
    weight = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'challenge_competence'


class ChallengeCompetenceEvaluated(models.Model):
    challenge = models.ForeignKey(Challenge, models.DO_NOTHING)
    competence = models.ForeignKey('Competence', models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'challenge_competence_evaluated'


class ChallengeHelpTopic(models.Model):
    challenge = models.ForeignKey(Challenge, models.DO_NOTHING)
    help_topic = models.ForeignKey('HelpTopic', models.DO_NOTHING)
    weight = models.FloatField()
    created_atc = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'challenge_help_topic'


class ChallengeLevel(models.Model):
    challenge = models.ForeignKey(Challenge, models.DO_NOTHING)
    name = models.CharField(max_length=45, db_collation='utf8mb4_unicode_ci')
    min_score = models.FloatField()
    max_score = models.FloatField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'challenge_level'


class ChallengeResource(models.Model):
    challenge = models.ForeignKey(Challenge, models.DO_NOTHING)
    resource = models.ForeignKey('Resource', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'challenge_resource'


class ChallengeRubric(models.Model):
    rubric = models.ForeignKey('Rubric', models.DO_NOTHING)
    challenge = models.ForeignKey(Challenge, models.DO_NOTHING)
    value = models.IntegerField()
    feedback = models.TextField(db_collation='utf8mb4_unicode_ci', blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)
    help_topic = models.ForeignKey('HelpTopic', models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'challenge_rubric'


class ChallengeVariable(models.Model):
    challenge = models.ForeignKey(Challenge, models.DO_NOTHING, blank=True, null=True)
    variable = models.ForeignKey('Variable', models.DO_NOTHING, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'challenge_variable'


class CheckList(models.Model):
    name = models.CharField(max_length=60)
    code = models.CharField(max_length=45)
    is_talent_pool = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'check_list'


class City(models.Model):
    name = models.CharField(max_length=45)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)
    country = models.ForeignKey('Country', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'city'


class CleaverCombinations(models.Model):
    dimension = models.CharField(max_length=45)
    type = models.CharField(max_length=45)
    combination = models.CharField(max_length=45)
    high_rank = models.CharField(max_length=45)
    medium_high_rank = models.CharField(max_length=45)
    medium_low_rank = models.CharField(max_length=45)
    low_rank = models.CharField(max_length=45)
    personality_description = models.TextField()
    job_description = models.TextField()
    category = models.CharField(max_length=45)
    thincrs_catalog = models.TextField()
    other_jobs = models.TextField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cleaver_combinations'


class CleaverPercentileReference(models.Model):
    dimension = models.CharField(max_length=45, blank=True, null=True)
    dimension_value = models.IntegerField(blank=True, null=True)
    percentile = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cleaver_percentile_reference'


class Comments(models.Model):
    enterprise_id = models.IntegerField()
    member_id = models.IntegerField()
    talent_id = models.IntegerField()
    type_user = models.IntegerField()
    comment_id = models.IntegerField(blank=True, null=True)
    comment = models.CharField(max_length=191)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'comments'


class Competence(models.Model):
    admin = models.ForeignKey(Admin, models.DO_NOTHING, blank=True, null=True)
    enterprise = models.ForeignKey('Enterprise', models.DO_NOTHING, blank=True, null=True)
    name = models.CharField(max_length=160, db_collation='utf8mb4_unicode_ci')
    name_en = models.CharField(max_length=160, blank=True, null=True)
    description = models.TextField(db_collation='utf8mb4_unicode_ci', blank=True, null=True)
    description_en = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)
    shortname = models.CharField(max_length=100, db_collation='utf8mb4_unicode_ci', blank=True, null=True)
    release = models.CharField(max_length=255, blank=True, null=True)
    status = models.CharField(max_length=10)

    class Meta:
        managed = False
        db_table = 'competence'


class CompetenceAssociated(models.Model):
    child_competence = models.ForeignKey(Competence, models.DO_NOTHING)
    father_competence = models.ForeignKey(Competence, models.DO_NOTHING)
    dependence = models.IntegerField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'competence_associated'


class CompetenceFeedback(models.Model):
    competence = models.ForeignKey(Competence, models.DO_NOTHING)
    feedback = models.ForeignKey('Feedback', models.DO_NOTHING)
    level = models.CharField(max_length=12)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'competence_feedback'


class CompetenceResource(models.Model):
    competence = models.ForeignKey(Competence, models.DO_NOTHING)
    resource = models.ForeignKey('Resource', models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'competence_resource'


class Context(models.Model):
    code = models.CharField(max_length=45)
    api = models.CharField(max_length=100)
    table_columns = models.TextField()
    order = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'context'


class Coordinator(models.Model):
    lastname = models.CharField(max_length=45, blank=True, null=True)
    user = models.ForeignKey('Users', models.DO_NOTHING)
    phone = models.CharField(max_length=45, blank=True, null=True)
    city = models.CharField(max_length=45, blank=True, null=True)
    state = models.CharField(max_length=45, blank=True, null=True)
    country = models.CharField(max_length=45, blank=True, null=True)
    enterprise = models.ForeignKey('Enterprise', models.DO_NOTHING)
    enabled = models.IntegerField()
    deleted_at = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'coordinator'


class CoordinatorGroup(models.Model):
    coordinator = models.ForeignKey(Coordinator, models.DO_NOTHING)
    group = models.ForeignKey('Group', models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'coordinator_group'


class Coordinators(models.Model):
    lastname = models.CharField(max_length=45, blank=True, null=True)
    user = models.ForeignKey('Users', models.DO_NOTHING)
    phone = models.CharField(max_length=45, blank=True, null=True)
    city = models.CharField(max_length=45, blank=True, null=True)
    state = models.CharField(max_length=45, blank=True, null=True)
    country = models.CharField(max_length=45, blank=True, null=True)
    university = models.ForeignKey('Universities', models.DO_NOTHING)
    deleted_at = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'coordinators'


class Country(models.Model):
    name = models.CharField(max_length=45)

    class Meta:
        managed = False
        db_table = 'country'


class Course(models.Model):
    admin_id = models.PositiveIntegerField(blank=True, null=True)
    image = models.CharField(max_length=100, db_collation='utf8mb4_unicode_ci')
    name = models.CharField(max_length=200, db_collation='utf8mb4_unicode_ci')
    short_name = models.CharField(max_length=45, db_collation='utf8mb4_unicode_ci')
    oss_project = models.CharField(max_length=45, db_collation='utf8mb4_unicode_ci')
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)
    version = models.IntegerField()
    description = models.TextField(db_collation='utf8mb4_unicode_ci', blank=True, null=True)
    summary = models.TextField(db_collation='utf8mb4_unicode_ci', blank=True, null=True)
    visible = models.IntegerField()
    course_type = models.ForeignKey('CourseType', models.DO_NOTHING)
    category = models.ForeignKey(Category, models.DO_NOTHING)
    duration_minutes = models.IntegerField()
    start_date = models.DateTimeField(blank=True, null=True)
    end_date = models.DateTimeField(blank=True, null=True)
    problematic = models.TextField(blank=True, null=True)
    challenge_number = models.IntegerField(blank=True, null=True)
    delivery_type = models.CharField(max_length=255, blank=True, null=True)
    enterprise = models.ForeignKey('Enterprise', models.DO_NOTHING, blank=True, null=True)
    status = models.CharField(max_length=10, blank=True, null=True)
    avatar = models.ForeignKey(Avatar, models.DO_NOTHING)
    relevance_level = models.IntegerField()
    verified_competences = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'course'


class CourseChallengeOrder(models.Model):
    course = models.ForeignKey(Course, models.DO_NOTHING)
    challenge = models.ForeignKey(Challenge, models.DO_NOTHING)
    position = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'course_challenge_order'


class CourseCompetence(models.Model):
    competence = models.ForeignKey(Competence, models.DO_NOTHING)
    course = models.ForeignKey(Course, models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)
    weight = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'course_competence'


class CourseEvaluator(models.Model):
    evaluator = models.ForeignKey('Evaluator', models.DO_NOTHING, blank=True, null=True)
    course = models.ForeignKey(Course, models.DO_NOTHING, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'course_evaluator'


class CourseHasInternship(models.Model):
    course = models.ForeignKey(Course, models.DO_NOTHING)
    internship_course = models.ForeignKey(Course, models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'course_has_internship'


class CourseIndustry(models.Model):
    course = models.ForeignKey(Course, models.DO_NOTHING)
    industry = models.ForeignKey('Industry', models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'course_industry'


class CourseResource(models.Model):
    course = models.ForeignKey(Course, models.DO_NOTHING)
    resource = models.ForeignKey('Resource', models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'course_resource'


class CourseSkill(models.Model):
    course = models.ForeignKey(Course, models.DO_NOTHING)
    skill = models.ForeignKey('Skill', models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'course_skill'


class CourseSkillCategory(models.Model):
    course = models.ForeignKey(Course, models.DO_NOTHING)
    skill_category = models.ForeignKey('SkillCategory', models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'course_skill_category'


class CourseTopic(models.Model):
    course = models.ForeignKey(Course, models.DO_NOTHING)
    topic = models.ForeignKey('Topic', models.DO_NOTHING)
    weight = models.FloatField()
    duration_minutes = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'course_topic'


class CourseTracingUnder(models.Model):
    course = models.ForeignKey(Course, models.DO_NOTHING)
    position = models.IntegerField(blank=True, null=True)
    required = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'course_tracing_under'


class CourseType(models.Model):
    name = models.CharField(max_length=45, db_collation='utf8mb4_unicode_ci')
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'course_type'


class CourseVariable(models.Model):
    course = models.ForeignKey(Course, models.DO_NOTHING)
    variable = models.ForeignKey('Variable', models.DO_NOTHING)
    value = models.CharField(max_length=255)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'course_variable'


class Credit(models.Model):
    enterprise = models.ForeignKey('Enterprise', models.DO_NOTHING, blank=True, null=True)
    credits = models.IntegerField()
    expire_in = models.DateTimeField(blank=True, null=True)
    status = models.CharField(max_length=7)
    detail = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)
    from_credit = models.ForeignKey('self', models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'credit'


class CreditPackage(models.Model):
    quantity = models.IntegerField()
    name = models.CharField(max_length=45)
    description = models.CharField(max_length=200, blank=True, null=True)
    price = models.FloatField()
    discount = models.FloatField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'credit_package'


class Credits(models.Model):
    talent = models.ForeignKey('Talent', models.DO_NOTHING, blank=True, null=True)
    credits = models.IntegerField(blank=True, null=True)
    detail = models.CharField(max_length=45, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'credits'


class CustomVariable(models.Model):
    talent_competence_challenge = models.ForeignKey('TalentChallengeCompetence', models.DO_NOTHING)
    value = models.CharField(max_length=160)
    name = models.CharField(max_length=45)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'custom_variable'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.PositiveSmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    id = models.BigAutoField(primary_key=True)
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class Document(models.Model):
    talent = models.ForeignKey('Talent', models.DO_NOTHING)
    file = models.TextField()
    document_type = models.ForeignKey('DocumentType', models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'document'


class DocumentType(models.Model):
    name = models.CharField(max_length=45, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'document_type'


class Education(models.Model):
    user = models.ForeignKey('Users', models.DO_NOTHING)
    lastname = models.CharField(max_length=100)
    enabled = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'education'


class EnglishResultsReference(models.Model):
    user_type = models.CharField(max_length=45)
    level = models.CharField(max_length=45)
    level_description = models.CharField(max_length=45)
    denomination = models.CharField(max_length=60)
    reference = models.TextField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'english_results_reference'


class Enterprise(models.Model):
    user = models.ForeignKey('Users', models.DO_NOTHING)
    type = models.CharField(max_length=30, blank=True, null=True)
    domain = models.CharField(max_length=200, blank=True, null=True)
    max_talents = models.IntegerField()
    address = models.CharField(max_length=150, blank=True, null=True)
    state = models.CharField(max_length=45, blank=True, null=True)
    city = models.CharField(max_length=45, blank=True, null=True)
    country = models.CharField(max_length=45, blank=True, null=True)
    website = models.CharField(max_length=100, blank=True, null=True)
    employees = models.CharField(max_length=20, blank=True, null=True)
    emp_six_month = models.IntegerField(blank=True, null=True)
    summary = models.TextField(blank=True, null=True)
    information = models.TextField(blank=True, null=True)
    acknowledgments = models.TextField(blank=True, null=True)
    benefits = models.TextField(blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    area_id = models.IntegerField(blank=True, null=True)
    personal_name = models.CharField(max_length=255, blank=True, null=True)
    lastname = models.CharField(max_length=255, blank=True, null=True)
    occupation = models.CharField(max_length=255, blank=True, null=True)
    wepowsubdomain = models.CharField(db_column='wepowSubdomain', max_length=255, blank=True, null=True)  # Field name made lowercase.
    stripe_customer = models.CharField(max_length=100, blank=True, null=True)
    udemy_project_id = models.PositiveIntegerField(blank=True, null=True)
    administrative_contact = models.TextField(blank=True, null=True)
    enable_extra_points = models.IntegerField()
    finished_project = models.IntegerField()
    industry = models.ForeignKey('Industry', models.DO_NOTHING, blank=True, null=True)
    phone = models.CharField(max_length=60, blank=True, null=True)
    institution_img = models.CharField(max_length=255, blank=True, null=True)
    onboarding = models.IntegerField()
    competency_gap = models.TextField(blank=True, null=True)
    training_process = models.TextField(blank=True, null=True)
    has_service_provider = models.IntegerField()
    service_provider_name = models.CharField(max_length=45, blank=True, null=True)
    competency_improve = models.CharField(max_length=45, blank=True, null=True)
    enable_rhh = models.IntegerField()
    enable_coordinators_udemy = models.IntegerField()
    enabled = models.IntegerField()
    is_qualifinds = models.IntegerField()
    is_upland = models.IntegerField()
    istalentpool = models.IntegerField(db_column='isTalentPool')  # Field name made lowercase.
    is_talent_pool_rh = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'enterprise'


class EnterpriseAreas(models.Model):
    enterprise_id = models.IntegerField()
    name = models.CharField(max_length=255)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'enterprise_areas'


class EnterpriseContext(models.Model):
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING)
    context = models.ForeignKey(Context, models.DO_NOTHING)
    order = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'enterprise_context'


class EnterpriseCourseGlobal(models.Model):
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING)
    course = models.ForeignKey(Course, models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'enterprise_course_global'


class EnterpriseCoursePoint(models.Model):
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING)
    course = models.ForeignKey(Course, models.DO_NOTHING)
    points = models.FloatField()
    percent = models.FloatField()
    extra_points = models.IntegerField()
    extra_rule = models.IntegerField(blank=True, null=True)
    basic = models.IntegerField(blank=True, null=True)
    intermediate = models.IntegerField(blank=True, null=True)
    advanced = models.IntegerField(blank=True, null=True)
    specialized = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'enterprise_course_point'


class EnterpriseEssentialTrait(models.Model):
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING)
    trait = models.ForeignKey('Trait', models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'enterprise_essential_trait'


class EnterpriseInterestSpecialization(models.Model):
    enterprise_id = models.PositiveIntegerField()
    specialization_id = models.PositiveIntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'enterprise_interest_specialization'


class EnterpriseMembers(models.Model):
    name = models.CharField(max_length=191, blank=True, null=True)
    lastname = models.CharField(max_length=191, blank=True, null=True)
    occupation = models.CharField(max_length=191, blank=True, null=True)
    user_id = models.IntegerField()
    enterprise_id = models.IntegerField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    area_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'enterprise_members'


class EnterpriseSubscription(models.Model):
    status = models.IntegerField()
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING)
    subscription = models.ForeignKey('Subscription', models.DO_NOTHING, blank=True, null=True)
    stripe_subscription = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'enterprise_subscription'


class EnterpriseSuggestTo(models.Model):
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING)
    suggest_to_enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'enterprise_suggest_to'


class EnterpriseWidget(models.Model):
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING)
    widget = models.ForeignKey('Widget', models.DO_NOTHING)
    order = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'enterprise_widget'


class EntityPermission(models.Model):
    member = models.ForeignKey('Member', models.DO_NOTHING, blank=True, null=True)
    talent = models.ForeignKey('Talent', models.DO_NOTHING, blank=True, null=True)
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING, blank=True, null=True)
    coordinator = models.ForeignKey(Coordinator, models.DO_NOTHING, blank=True, null=True)
    evaluator = models.ForeignKey('Evaluator', models.DO_NOTHING, blank=True, null=True)
    support = models.ForeignKey('Support', models.DO_NOTHING, blank=True, null=True)
    education = models.ForeignKey(Education, models.DO_NOTHING, blank=True, null=True)
    executive = models.ForeignKey('Executive', models.DO_NOTHING, blank=True, null=True)
    permission = models.CharField(max_length=13)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'entity_permission'


class Evaluation(models.Model):
    admin = models.ForeignKey(Admin, models.DO_NOTHING, blank=True, null=True)
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING, blank=True, null=True)
    name = models.CharField(max_length=160, db_collation='utf8mb4_unicode_ci')
    description = models.TextField(db_collation='utf8mb4_unicode_ci', blank=True, null=True)
    instructions = models.TextField(db_collation='utf8mb4_unicode_ci', blank=True, null=True)
    start_date = models.DateTimeField(blank=True, null=True)
    end_date = models.DateTimeField(blank=True, null=True)
    limit_time = models.IntegerField()
    min_score = models.FloatField()
    weight = models.IntegerField(blank=True, null=True)
    max_intents = models.IntegerField()
    random = models.IntegerField()
    random_answers = models.IntegerField()
    points = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)
    duration_minutes = models.IntegerField()
    evaluation_type = models.ForeignKey('EvaluationType', models.DO_NOTHING)
    smart = models.IntegerField()
    smart_level = models.IntegerField()
    status = models.CharField(max_length=10)
    micro = models.IntegerField()
    max_micro = models.IntegerField()
    display_type = models.CharField(max_length=50, blank=True, null=True)
    relevance_level = models.IntegerField()
    evaluation_id_reference = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'evaluation'


class EvaluationCategory(models.Model):
    talent_evaluation_competence = models.ForeignKey('TalentEvaluationCompetence', models.DO_NOTHING)
    question_category = models.ForeignKey('QuestionCategory', models.DO_NOTHING)
    weight = models.FloatField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'evaluation_category'


class EvaluationCategoryFeedback(models.Model):
    feedback = models.ForeignKey('Feedback', models.DO_NOTHING)
    evaluation_category = models.ForeignKey(EvaluationCategory, models.DO_NOTHING)
    is_correct = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'evaluation_category_feedback'


class EvaluationCategoryFeedbackResource(models.Model):
    evaluation_category_feedback = models.ForeignKey(EvaluationCategoryFeedback, models.DO_NOTHING)
    resource = models.ForeignKey('Resource', models.DO_NOTHING)
    flag = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'evaluation_category_feedback_resource'


class EvaluationCompetence(models.Model):
    competence = models.ForeignKey(Competence, models.DO_NOTHING)
    evaluation = models.ForeignKey(Evaluation, models.DO_NOTHING)
    weight = models.FloatField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'evaluation_competence'


class EvaluationCompetenceEvaluated(models.Model):
    evaluation = models.ForeignKey(Evaluation, models.DO_NOTHING)
    competence = models.ForeignKey(Competence, models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'evaluation_competence_evaluated'


class EvaluationHasMicro(models.Model):
    evaluation = models.ForeignKey(Evaluation, models.DO_NOTHING)
    micro_evaluation = models.ForeignKey(Evaluation, models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'evaluation_has_micro'


class EvaluationMethod(models.Model):
    evaluation = models.ForeignKey(Evaluation, models.DO_NOTHING)
    name = models.CharField(max_length=45)
    action_method = models.ForeignKey(ActionMethod, models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'evaluation_method'


class EvaluationQuestion(models.Model):
    evaluation = models.ForeignKey(Evaluation, models.DO_NOTHING)
    question = models.ForeignKey('Question', models.DO_NOTHING)
    order = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'evaluation_question'


class EvaluationRule(models.Model):
    action_rule = models.ForeignKey(ActionRule, models.DO_NOTHING)
    name = models.CharField(max_length=45)
    evaluation = models.ForeignKey(Evaluation, models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'evaluation_rule'


class EvaluationType(models.Model):
    name = models.CharField(max_length=45)
    prefix_url = models.CharField(max_length=3)
    variant = models.CharField(max_length=5, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'evaluation_type'


class EvaluationTypeAlternative(models.Model):
    name = models.CharField(max_length=45)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'evaluation_type_alternative'


class Evaluator(models.Model):
    lastname = models.CharField(max_length=200, blank=True, null=True)
    user = models.ForeignKey('Users', models.DO_NOTHING)
    phone = models.CharField(max_length=45, blank=True, null=True)
    city = models.CharField(max_length=45, blank=True, null=True)
    state = models.CharField(max_length=45, blank=True, null=True)
    country = models.CharField(max_length=45, blank=True, null=True)
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING)
    enabled = models.IntegerField()
    deleted_at = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)
    rating = models.FloatField()

    class Meta:
        managed = False
        db_table = 'evaluator'


class Executive(models.Model):
    lastname = models.CharField(max_length=45, blank=True, null=True)
    user = models.ForeignKey('Users', models.DO_NOTHING)
    phone = models.CharField(max_length=45, blank=True, null=True)
    city = models.CharField(max_length=45, blank=True, null=True)
    state = models.CharField(max_length=45, blank=True, null=True)
    country = models.CharField(max_length=45, blank=True, null=True)
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING)
    enabled = models.IntegerField()
    deleted_at = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'executive'


class ExecutiveEnterpriseSuggest(models.Model):
    executive = models.ForeignKey(Executive, models.DO_NOTHING)
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'executive_enterprise_suggest'


class ExecutiveGroup(models.Model):
    executive = models.ForeignKey(Executive, models.DO_NOTHING)
    group = models.ForeignKey('Group', models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'executive_group'


class Feedback(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    description = models.TextField(db_collation='utf8mb4_unicode_ci')
    admin = models.ForeignKey(Admin, models.DO_NOTHING, blank=True, null=True)
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)
    feedback_type = models.ForeignKey('FeedbackType', models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'feedback'


class FeedbackResource(models.Model):
    feedback = models.ForeignKey(Feedback, models.DO_NOTHING)
    resource = models.ForeignKey('Resource', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'feedback_resource'


class FeedbackType(models.Model):
    name = models.CharField(max_length=45)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'feedback_type'


class File(models.Model):
    path = models.CharField(max_length=100)
    filename = models.CharField(max_length=45)
    size = models.CharField(max_length=45, blank=True, null=True)
    mimetype = models.CharField(max_length=45)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)
    user = models.ForeignKey('Users', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'file'


class GraphAreaPercentSkill(models.Model):
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING)
    short_name = models.CharField(max_length=200, blank=True, null=True)
    course = models.ForeignKey(Course, models.DO_NOTHING, blank=True, null=True)
    group_id = models.PositiveIntegerField(blank=True, null=True)
    advanced = models.TextField()
    basic = models.TextField()
    intermediate = models.TextField()
    specialized = models.TextField()
    not_rated = models.TextField()
    table_headers = models.TextField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'graph_area_percent_skill'


class GraphAverageCompetence(models.Model):
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING, blank=True, null=True)
    course_id = models.PositiveIntegerField()
    group_id = models.PositiveIntegerField(blank=True, null=True)
    data = models.TextField()
    weight = models.FloatField()
    level = models.CharField(max_length=45)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'graph_average_competence'


class GraphCertificationCompleted(models.Model):
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING)
    short_name = models.CharField(max_length=200)
    course = models.ForeignKey(Course, models.DO_NOTHING, blank=True, null=True)
    group_id = models.PositiveIntegerField(blank=True, null=True)
    certified_data = models.TextField()
    finished_data = models.TextField()
    months = models.TextField()
    years = models.TextField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'graph_certification_completed'


class GraphFunnel(models.Model):
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING)
    course_id = models.PositiveIntegerField()
    group_id = models.PositiveIntegerField(blank=True, null=True)
    data = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'graph_funnel'


class GraphGlobalLevel(models.Model):
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING)
    short_name = models.CharField(max_length=200)
    course = models.ForeignKey(Course, models.DO_NOTHING, blank=True, null=True)
    group_id = models.PositiveIntegerField(blank=True, null=True)
    percent_advanced = models.CharField(max_length=45)
    percent_basic = models.CharField(max_length=45)
    percent_intermediate = models.CharField(max_length=45)
    percent_specialized = models.CharField(max_length=45)
    tot_advanced = models.IntegerField()
    tot_basic = models.IntegerField()
    tot_intermediate = models.IntegerField()
    tot_specialized = models.IntegerField()
    total = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'graph_global_level'


class GraphGlobalPercentSkill(models.Model):
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING)
    short_name = models.CharField(max_length=200)
    course = models.ForeignKey(Course, models.DO_NOTHING, blank=True, null=True)
    group_id = models.PositiveIntegerField(blank=True, null=True)
    skill_name = models.CharField(max_length=200)
    finished_percent = models.CharField(max_length=45)
    tot_finished = models.CharField(max_length=45)
    tot_unfinished = models.CharField(max_length=45)
    unfinished_percent = models.CharField(max_length=45)
    created_at = models.DateTimeField()
    updated_at = models.CharField(max_length=45, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'graph_global_percent_skill'


class GraphGlobalSkillLevel(models.Model):
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING)
    short_name = models.CharField(max_length=200)
    course = models.ForeignKey(Course, models.DO_NOTHING, blank=True, null=True)
    group_id = models.PositiveIntegerField(blank=True, null=True)
    skill_name = models.CharField(max_length=200)
    percent = models.CharField(max_length=45)
    level_eng = models.CharField(max_length=45)
    level_esp = models.CharField(max_length=45)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'graph_global_skill_level'


class GraphTopGroup(models.Model):
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING)
    data = models.TextField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'graph_top_group'


class GraphTopTalentGroup(models.Model):
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING)
    group = models.ForeignKey('Group', models.DO_NOTHING)
    data = models.TextField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'graph_top_talent_group'


class GraphTotalsCandidate(models.Model):
    enterprise_id = models.PositiveIntegerField(blank=True, null=True)
    executive_id = models.PositiveIntegerField(blank=True, null=True)
    data = models.TextField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'graph_totals_candidate'


class GraphTotalsCompetence(models.Model):
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING)
    data = models.TextField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'graph_totals_competence'


class GraphUsersCompleted(models.Model):
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING)
    data = models.TextField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'graph_users_completed'


class Group(models.Model):
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING)
    name = models.CharField(max_length=45, blank=True, null=True)
    summary = models.TextField(blank=True, null=True)
    register_code = models.CharField(max_length=45)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    career = models.CharField(max_length=190, blank=True, null=True)
    subject = models.CharField(max_length=45, blank=True, null=True)
    campus = models.CharField(max_length=100, blank=True, null=True)
    max_talents = models.IntegerField(blank=True, null=True)
    coordinator = models.ForeignKey(Coordinator, models.DO_NOTHING, blank=True, null=True)
    course = models.ForeignKey(Course, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'group'


class GroupCampusConfig(models.Model):
    name = models.CharField(max_length=45, blank=True, null=True)
    university_id = models.CharField(max_length=45, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'group_campus_config'


class GroupCareerConfig(models.Model):
    name = models.CharField(max_length=45, blank=True, null=True)
    university_id = models.CharField(max_length=45, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'group_career_config'


class GroupCourse(models.Model):
    group = models.ForeignKey(Group, models.DO_NOTHING)
    course = models.ForeignKey(Course, models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'group_course'


class GroupDescriptionsConfig(models.Model):
    name = models.CharField(max_length=45, blank=True, null=True)
    university_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'group_descriptions_config'


class GroupInvitation(models.Model):
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    name = models.CharField(max_length=70, db_collation='utf8mb4_unicode_ci', blank=True, null=True)
    lastname = models.CharField(max_length=70, db_collation='utf8mb4_unicode_ci', blank=True, null=True)
    email = models.CharField(max_length=200, db_collation='utf8mb4_unicode_ci', blank=True, null=True)
    phone = models.CharField(max_length=45, blank=True, null=True)
    group = models.ForeignKey(Group, models.DO_NOTHING)
    flag = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'group_invitation'


class GroupLabelsConfig(models.Model):
    name = models.CharField(max_length=45, blank=True, null=True)
    university_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'group_labels_config'


class GroupSubjectConfig(models.Model):
    name = models.CharField(max_length=45, blank=True, null=True)
    university_id = models.CharField(max_length=45, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'group_subject_config'


class HelpTopic(models.Model):
    name = models.CharField(max_length=200, db_collation='utf8mb4_0900_ai_ci')
    description = models.TextField(db_collation='utf8mb4_0900_ai_ci')
    name_en = models.CharField(max_length=200, blank=True, null=True)
    description_en = models.TextField(blank=True, null=True)
    competence = models.ForeignKey(Competence, models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'help_topic'


class HelpTopicRubric(models.Model):
    help_topic = models.ForeignKey(HelpTopic, models.DO_NOTHING)
    rubric = models.ForeignKey('Rubric', models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'help_topic_rubric'


class HiringOffers(models.Model):
    verification_id = models.CharField(max_length=45)
    interview_id = models.CharField(max_length=45)
    talent_id = models.CharField(max_length=45)
    enterprise_id = models.CharField(max_length=45)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    last_step = models.CharField(max_length=45)

    class Meta:
        managed = False
        db_table = 'hiring_offers'


class Image(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField()
    file = models.CharField(max_length=100)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'image'


class InProject(models.Model):
    udemy = models.ForeignKey('Udemy', models.DO_NOTHING)
    udemy_project = models.ForeignKey('UdemyProjects', models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'in_project'


class Industry(models.Model):
    name = models.CharField(max_length=45)
    name_en = models.CharField(max_length=45, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'industry'


class Institution(models.Model):
    federal_entity = models.CharField(max_length=100)
    subsystem = models.CharField(max_length=100)
    institution_name = models.CharField(max_length=200)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'institution'


class InstitutionCareer(models.Model):
    career = models.CharField(max_length=150)
    category_career = models.CharField(max_length=200)
    institution_id = models.PositiveIntegerField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'institution_career'


class InterviewInvitations(models.Model):
    enterprise_id = models.IntegerField()
    talent_id = models.IntegerField()
    step_id = models.IntegerField()
    token = models.CharField(max_length=255)
    status = models.IntegerField()
    evaluator_name = models.CharField(max_length=255)
    evaluator_email = models.CharField(max_length=255)
    score = models.IntegerField(blank=True, null=True)
    comment = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    interview_link = models.CharField(max_length=255, blank=True, null=True)
    interview_id = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'interview_invitations'


class Interviews(models.Model):
    enterprise_id = models.IntegerField()
    name = models.CharField(max_length=191)
    contact_name = models.CharField(max_length=191, blank=True, null=True)
    order = models.IntegerField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'interviews'


class Invitation(models.Model):
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING)
    email = models.CharField(max_length=100)
    name = models.CharField(max_length=60)
    lastname = models.CharField(max_length=60, blank=True, null=True)
    phone = models.CharField(max_length=45, blank=True, null=True)
    used = models.IntegerField()
    user_id = models.PositiveIntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'invitation'


class InvitationGroup(models.Model):
    invitation = models.ForeignKey(Invitation, models.DO_NOTHING)
    group = models.ForeignKey(Group, models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'invitation_group'


class IssueReqs(models.Model):
    talent = models.ForeignKey('Talent', models.DO_NOTHING, blank=True, null=True)
    issue_request = models.ForeignKey('IssueRequests', models.DO_NOTHING, blank=True, null=True)
    detail = models.CharField(max_length=45, blank=True, null=True)
    course = models.CharField(max_length=45, blank=True, null=True)
    solved = models.CharField(max_length=45, blank=True, null=True)
    points = models.CharField(max_length=45, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'issue_reqs'


class IssueRequests(models.Model):
    sumary = models.CharField(max_length=45, blank=True, null=True)
    issue_key = models.CharField(max_length=45, blank=True, null=True)
    issue_id = models.CharField(max_length=45, blank=True, null=True)
    issue_type = models.CharField(max_length=45, blank=True, null=True)
    status = models.CharField(max_length=45, blank=True, null=True)
    project = models.CharField(max_length=45, blank=True, null=True)
    priority = models.CharField(max_length=45, blank=True, null=True)
    resolution = models.CharField(max_length=45, blank=True, null=True)
    assignee = models.CharField(max_length=45, blank=True, null=True)
    labels = models.CharField(max_length=45, blank=True, null=True)
    date_created = models.CharField(max_length=45, blank=True, null=True)
    date_updated = models.CharField(max_length=45, blank=True, null=True)
    available = models.IntegerField(blank=True, null=True)
    level = models.CharField(max_length=45, blank=True, null=True)
    sponsored = models.CharField(max_length=45, blank=True, null=True)
    limit_date = models.DateTimeField(blank=True, null=True)
    talent = models.ForeignKey('Talent', models.DO_NOTHING, blank=True, null=True)
    course = models.ForeignKey(Course, models.DO_NOTHING, blank=True, null=True)
    points = models.IntegerField(blank=True, null=True)
    link = models.CharField(max_length=45, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'issue_requests'


class Issues(models.Model):
    sumary = models.CharField(max_length=200, blank=True, null=True)
    issue_key = models.CharField(max_length=45, blank=True, null=True)
    issue_id = models.CharField(max_length=45, blank=True, null=True)
    issue_type = models.CharField(max_length=45, blank=True, null=True)
    status = models.CharField(max_length=45, blank=True, null=True)
    project = models.CharField(max_length=45, blank=True, null=True)
    priority = models.CharField(max_length=45, blank=True, null=True)
    resolution = models.CharField(max_length=45, blank=True, null=True)
    assignee = models.CharField(max_length=45, blank=True, null=True)
    labels = models.CharField(max_length=45, blank=True, null=True)
    date_created = models.CharField(max_length=45, blank=True, null=True)
    date_updated = models.CharField(max_length=45, blank=True, null=True)
    available = models.IntegerField(blank=True, null=True)
    level = models.CharField(max_length=45, blank=True, null=True)
    sponsored = models.CharField(max_length=45, blank=True, null=True)
    link = models.CharField(max_length=45, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'issues'


class Job(models.Model):
    job_type = models.ForeignKey('JobType', models.DO_NOTHING, blank=True, null=True)
    city = models.ForeignKey(City, models.DO_NOTHING, blank=True, null=True)
    name = models.CharField(max_length=45, blank=True, null=True)
    is_remote = models.IntegerField()
    origin_enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING)
    destination_enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING)
    start_date = models.DateField()
    end_date = models.DateField(blank=True, null=True)
    max_talents = models.IntegerField()
    total_vacancies = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'job'


class JobCompetence(models.Model):
    int = models.AutoField(primary_key=True)
    job = models.ForeignKey(Job, models.DO_NOTHING)
    competence = models.ForeignKey(Competence, models.DO_NOTHING)
    weight = models.FloatField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'job_competence'


class JobEvaluator(models.Model):
    job = models.ForeignKey(Job, models.DO_NOTHING)
    evaluator = models.ForeignKey(Evaluator, models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'job_evaluator'


class JobExecutive(models.Model):
    job = models.ForeignKey(Job, models.DO_NOTHING)
    executive = models.ForeignKey(Executive, models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'job_executive'


class JobSpecialization(models.Model):
    job = models.ForeignKey(Job, models.DO_NOTHING)
    specialization = models.ForeignKey('Specialization', models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'job_specialization'


class JobType(models.Model):
    name = models.CharField(max_length=45, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'job_type'


class Jobs(models.Model):
    id = models.BigAutoField(primary_key=True)
    queue = models.CharField(max_length=45, blank=True, null=True)
    payload = models.TextField(blank=True, null=True)
    attempts = models.PositiveIntegerField(blank=True, null=True)
    reserved_at = models.PositiveIntegerField(blank=True, null=True)
    available_at = models.PositiveIntegerField(blank=True, null=True)
    created_at = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'jobs'


class Languages(models.Model):
    name = models.CharField(max_length=50)
    code = models.CharField(max_length=45)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'languages'


class LanguagesTranslate(models.Model):
    language = models.ForeignKey(Languages, models.DO_NOTHING)
    translate = models.ForeignKey('Translate', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'languages_translate'


class LearningPath(models.Model):
    talent_id = models.PositiveIntegerField()
    course_id_selected = models.PositiveIntegerField()
    data = models.TextField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'learning_path'


class Level(models.Model):
    evaluation = models.ForeignKey(Evaluation, models.DO_NOTHING)
    name = models.CharField(max_length=45, db_collation='utf8mb4_0900_ai_ci')
    min_score = models.FloatField()
    max_score = models.FloatField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'level'


class Member(models.Model):
    lastname = models.CharField(max_length=60)
    user = models.ForeignKey('Users', models.DO_NOTHING)
    city = models.CharField(max_length=45, blank=True, null=True)
    state = models.CharField(max_length=45, blank=True, null=True)
    country = models.CharField(max_length=45, blank=True, null=True)
    phone = models.CharField(max_length=45, blank=True, null=True)
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING)
    hidden = models.IntegerField()
    enabled = models.IntegerField()
    deleted_at = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'member'


class MemberAreas(models.Model):
    member_id = models.IntegerField()
    area_id = models.IntegerField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'member_areas'


class Migrations(models.Model):
    migration = models.CharField(max_length=191)
    batch = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'migrations'


class Note(models.Model):
    talent = models.ForeignKey('Talent', models.DO_NOTHING)
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING, blank=True, null=True)
    executive = models.ForeignKey(Executive, models.DO_NOTHING, blank=True, null=True)
    evaluator_id = models.PositiveIntegerField(blank=True, null=True)
    note = models.TextField()
    is_public = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)
    message_thincrs = models.IntegerField()
    messsage_type = models.CharField(max_length=8)

    class Meta:
        managed = False
        db_table = 'note'


class OauthAccessTokens(models.Model):
    id = models.CharField(primary_key=True, max_length=100)
    user_id = models.IntegerField(blank=True, null=True)
    client_id = models.IntegerField()
    name = models.CharField(max_length=191, blank=True, null=True)
    scopes = models.TextField(blank=True, null=True)
    revoked = models.IntegerField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    expires_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'oauth_access_tokens'


class OauthAuthCodes(models.Model):
    id = models.CharField(primary_key=True, max_length=100)
    user_id = models.IntegerField()
    client_id = models.IntegerField()
    scopes = models.TextField(blank=True, null=True)
    revoked = models.IntegerField()
    expires_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'oauth_auth_codes'


class OauthClients(models.Model):
    user_id = models.IntegerField(blank=True, null=True)
    name = models.CharField(max_length=191)
    secret = models.CharField(max_length=100)
    provider = models.CharField(max_length=100, blank=True, null=True)
    redirect = models.TextField()
    personal_access_client = models.IntegerField()
    password_client = models.IntegerField()
    revoked = models.IntegerField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'oauth_clients'


class OauthPersonalAccessClients(models.Model):
    client_id = models.IntegerField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'oauth_personal_access_clients'


class OauthRefreshTokens(models.Model):
    id = models.CharField(primary_key=True, max_length=100)
    access_token_id = models.CharField(max_length=100)
    revoked = models.IntegerField()
    expires_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'oauth_refresh_tokens'


class Onboarding(models.Model):
    url = models.CharField(max_length=200)
    course_type = models.ForeignKey(CourseType, models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'onboarding'


class Orientation(models.Model):
    name = models.CharField(max_length=45, blank=True, null=True)
    visible = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'orientation'


class PasswordResets(models.Model):
    email = models.CharField(max_length=191)
    token = models.CharField(max_length=191)
    created_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'password_resets'


class Path(models.Model):
    course = models.ForeignKey(Course, models.DO_NOTHING)
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING, blank=True, null=True)
    admin_id = models.PositiveIntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'path'


class PathStep(models.Model):
    path = models.ForeignKey(Path, models.DO_NOTHING)
    number = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'path_step'


class PendingModules(models.Model):
    talent = models.ForeignKey('Talent', models.DO_NOTHING, blank=True, null=True)
    talent_email = models.CharField(max_length=45, blank=True, null=True)
    course_shortname = models.CharField(max_length=45, blank=True, null=True)
    num_module = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pending_modules'


class Points(models.Model):
    talent = models.ForeignKey('Talent', models.DO_NOTHING, blank=True, null=True)
    points = models.IntegerField(blank=True, null=True)
    detail = models.CharField(max_length=45, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'points'


class ProfessionalCompetence(models.Model):
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING)
    name = models.CharField(max_length=80)
    is_ondemand = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'professional_competence'


class Provider(models.Model):
    name = models.CharField(max_length=45, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'provider'


class Question(models.Model):
    question_type = models.ForeignKey('QuestionType', models.DO_NOTHING)
    admin = models.ForeignKey(Admin, models.DO_NOTHING, blank=True, null=True)
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING, blank=True, null=True)
    short_name = models.CharField(max_length=45, db_collation='utf8mb4_0900_ai_ci')
    question = models.TextField(db_collation='utf8mb4_0900_ai_ci')
    points = models.FloatField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)
    question_category_id = models.IntegerField(blank=True, null=True)
    level = models.CharField(max_length=1, db_collation='utf8mb4_0900_ai_ci', blank=True, null=True)
    instructions = models.CharField(max_length=255, db_collation='utf8mb4_0900_ai_ci', blank=True, null=True)
    code = models.CharField(max_length=45, db_collation='utf8mb4_0900_ai_ci', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'question'


class QuestionCategory(models.Model):
    name = models.CharField(max_length=100)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'question_category'


class QuestionCategorySkill(models.Model):
    question_category = models.ForeignKey(QuestionCategory, models.DO_NOTHING)
    skill = models.ForeignKey('Skill', models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'question_category_skill'


class QuestionCompetence(models.Model):
    question = models.ForeignKey(Question, models.DO_NOTHING)
    competence = models.ForeignKey(Competence, models.DO_NOTHING)
    order = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'question_competence'


class QuestionFeedback(models.Model):
    question = models.ForeignKey(Question, models.DO_NOTHING)
    feedback = models.ForeignKey(Feedback, models.DO_NOTHING)
    is_correct = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'question_feedback'


class QuestionType(models.Model):
    type = models.CharField(max_length=45)
    name = models.TextField()
    image = models.CharField(max_length=200, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'question_type'


class RecruitmentProspecting(models.Model):
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING)
    name = models.CharField(max_length=45)
    order = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'recruitment_prospecting'


class ReportEnterpriseTalentCompetenceResultDetail(models.Model):
    enterprise_id = models.IntegerField()
    is_assigned = models.IntegerField()
    talent_id = models.IntegerField()
    group_id = models.IntegerField(blank=True, null=True)
    course_id = models.IntegerField()
    element_id = models.IntegerField()
    element_type = models.CharField(max_length=45)
    element_name = models.CharField(max_length=100)
    competence_id = models.IntegerField()
    weight = models.FloatField()
    level = models.CharField(max_length=45)
    weight_updated_at = models.DateTimeField(blank=True, null=True)
    elements = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'report_enterprise_talent_competence_result_detail'


class ReportEnterpriseTalentCourseElementsStatus(models.Model):
    enterprise_id = models.IntegerField()
    is_assigned = models.IntegerField()
    talent_id = models.IntegerField()
    group_id = models.IntegerField(blank=True, null=True)
    course_id = models.IntegerField()
    element_id = models.IntegerField()
    element_type = models.CharField(max_length=45)
    element_name = models.CharField(max_length=100)
    element_status = models.CharField(max_length=100)
    element_updated_at = models.DateTimeField()
    element_weight = models.FloatField()
    competence_id = models.IntegerField()
    competence_weight = models.FloatField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'report_enterprise_talent_course_elements_status'


class ReportTalentCourseResultDetail(models.Model):
    talent_id = models.CharField(max_length=45)
    course_id = models.CharField(max_length=45)
    enterprise_id = models.CharField(max_length=45, blank=True, null=True)
    is_assigned = models.IntegerField()
    group_id = models.CharField(max_length=45, blank=True, null=True)
    date = models.DateField(blank=True, null=True)
    global_result = models.CharField(max_length=45)
    global_level = models.CharField(max_length=45)
    status = models.CharField(max_length=45)
    elements = models.TextField()
    acreditation_date = models.DateField(blank=True, null=True)
    acquired_competences = models.IntegerField()
    tot_competences = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'report_talent_course_result_detail'


class Resource(models.Model):
    name = models.CharField(max_length=200, db_collation='utf8mb4_0900_ai_ci')
    alias = models.CharField(max_length=200, blank=True, null=True)
    url = models.CharField(max_length=300, db_collation='utf8mb4_0900_ai_ci')
    type = models.CharField(max_length=5)
    language = models.CharField(max_length=32, db_collation='utf8mb4_0900_ai_ci', blank=True, null=True)
    provider = models.ForeignKey(Provider, models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'resource'


class ResourceType(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=5, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'resource_type'


class RotatedPosition(models.Model):
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING)
    name = models.CharField(max_length=60)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rotated_position'


class RotationCause(models.Model):
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING)
    name = models.CharField(max_length=60)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rotation_cause'


class Rubric(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField(blank=True, null=True)
    name_en = models.CharField(max_length=200, blank=True, null=True)
    description_en = models.TextField(blank=True, null=True)
    competence = models.ForeignKey(Competence, models.DO_NOTHING, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)
    admin = models.ForeignKey(Admin, models.DO_NOTHING, blank=True, null=True)
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rubric'


class RubricFeedback(models.Model):
    rubric = models.ForeignKey(Rubric, models.DO_NOTHING)
    feedback = models.ForeignKey(Feedback, models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'rubric_feedback'


class Scope(models.Model):
    name = models.CharField(max_length=45)

    class Meta:
        managed = False
        db_table = 'scope'


class SignupEnterpriseForm(models.Model):
    int = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)
    email = models.CharField(max_length=45)
    type = models.CharField(max_length=45)
    enterprise_name = models.CharField(max_length=200, blank=True, null=True)
    desired = models.CharField(max_length=200, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'signup_enterprise_form'


class Skill(models.Model):
    name = models.CharField(max_length=50, db_collation='utf8mb4_unicode_ci')
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)
    type = models.CharField(max_length=45, db_collation='utf8mb4_unicode_ci')
    weight = models.FloatField(blank=True, null=True)
    skill_type = models.ForeignKey('SkillType', models.DO_NOTHING)
    release = models.CharField(max_length=255, db_collation='utf8mb4_unicode_ci', blank=True, null=True)
    shortname = models.CharField(max_length=255, db_collation='utf8mb4_unicode_ci', blank=True, null=True)
    tageable = models.CharField(max_length=255, db_collation='utf8mb4_unicode_ci', blank=True, null=True)
    skill_category = models.ForeignKey('SkillCategory', models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'skill'


class SkillCategory(models.Model):
    name = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'skill_category'


class SkillOrientation(models.Model):
    skill_id = models.PositiveIntegerField()
    orientation_id = models.PositiveIntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'skill_orientation'


class SkillResource(models.Model):
    skill = models.ForeignKey(Skill, models.DO_NOTHING)
    resource = models.ForeignKey(Resource, models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'skill_resource'


class SkillType(models.Model):
    name = models.CharField(max_length=45)

    class Meta:
        managed = False
        db_table = 'skill_type'


class SocialNetwork(models.Model):
    name = models.CharField(max_length=45)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'social_network'


class SpecialUsers(models.Model):
    email = models.CharField(max_length=45, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'special_users'


class Specialization(models.Model):
    name = models.CharField(max_length=60)
    shortname = models.CharField(max_length=45, blank=True, null=True)
    available = models.IntegerField()
    talent_pool_course = models.ForeignKey(Course, models.DO_NOTHING, blank=True, null=True)
    bootcamp_course = models.ForeignKey(Course, models.DO_NOTHING, blank=True, null=True)
    course = models.ForeignKey(Course, models.DO_NOTHING, blank=True, null=True)
    orientation_id = models.PositiveIntegerField()
    category_id = models.PositiveIntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)
    jobs = models.IntegerField(blank=True, null=True)
    salary = models.CharField(max_length=45, blank=True, null=True)
    bootcamp = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'specialization'


class Specialties(models.Model):
    name = models.CharField(max_length=50)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'specialties'


class SponsoredIssues(models.Model):
    issue_key = models.CharField(max_length=45, blank=True, null=True)
    issue_id = models.CharField(max_length=45, blank=True, null=True)
    sumary = models.CharField(max_length=45, blank=True, null=True)
    project = models.CharField(max_length=45, blank=True, null=True)
    level = models.CharField(max_length=45, blank=True, null=True)
    enterprise = models.ForeignKey('OldEnterprises', models.DO_NOTHING, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'sponsored_issues'


class StepElement(models.Model):
    path_step = models.ForeignKey(PathStep, models.DO_NOTHING)
    course = models.ForeignKey(Course, models.DO_NOTHING, blank=True, null=True)
    evaluation = models.ForeignKey(Evaluation, models.DO_NOTHING, blank=True, null=True)
    order = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'step_element'


class StripeProduct(models.Model):
    name = models.CharField(max_length=45, blank=True, null=True)
    product = models.CharField(max_length=60)
    type = models.CharField(max_length=10)
    display = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'stripe_product'


class Studies(models.Model):
    talent = models.ForeignKey('Talent', models.DO_NOTHING)
    institute_name = models.CharField(max_length=191)
    start_date = models.DateField(blank=True, null=True)
    end_date = models.DateField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'studies'


class Subscription(models.Model):
    name = models.CharField(max_length=45)
    code = models.CharField(max_length=9)
    stripe_plan = models.CharField(max_length=100)
    scope = models.ForeignKey(Scope, models.DO_NOTHING)
    credits = models.IntegerField()
    frequency = models.CharField(max_length=7)

    class Meta:
        managed = False
        db_table = 'subscription'


class Subscriptions(models.Model):
    name = models.CharField(max_length=191)
    price = models.FloatField()
    visible = models.IntegerField()
    deleted_at = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    identifier = models.CharField(max_length=191)

    class Meta:
        managed = False
        db_table = 'subscriptions'


class Support(models.Model):
    lastname = models.CharField(max_length=45)
    user = models.ForeignKey('Users', models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'support'


class SystemModule(models.Model):
    name = models.CharField(max_length=60)
    name_en = models.CharField(max_length=60, blank=True, null=True)
    short_name = models.CharField(max_length=45, blank=True, null=True)
    system = models.CharField(max_length=10)
    is_view = models.IntegerField()
    scopes = models.CharField(max_length=100, blank=True, null=True)
    route = models.CharField(max_length=45, blank=True, null=True)
    icon = models.CharField(max_length=45, blank=True, null=True)
    system_module = models.ForeignKey('self', models.DO_NOTHING, blank=True, null=True)
    order = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'system_module'


class SystemModuleSubscription(models.Model):
    system_module = models.ForeignKey(SystemModule, models.DO_NOTHING)
    subscription = models.ForeignKey(Subscription, models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'system_module_subscription'


class Talent(models.Model):
    lastname = models.CharField(max_length=200, blank=True, null=True)
    user = models.ForeignKey('Users', models.DO_NOTHING)
    stripe_id = models.CharField(max_length=100, blank=True, null=True)
    stripe_subscription = models.CharField(max_length=100, blank=True, null=True)
    stripe_plan = models.CharField(max_length=100, blank=True, null=True)
    conekta_id = models.CharField(max_length=100, blank=True, null=True)
    conekta_subscription = models.CharField(max_length=100, blank=True, null=True)
    conekta_plan = models.CharField(max_length=100, blank=True, null=True)
    city = models.CharField(max_length=191, blank=True, null=True)
    state = models.CharField(max_length=191, blank=True, null=True)
    country = models.CharField(max_length=191, blank=True, null=True)
    birthdate = models.DateField(blank=True, null=True)
    extrac_talent = models.TextField(blank=True, null=True)
    experience = models.IntegerField(blank=True, null=True)
    role_work = models.CharField(max_length=191, blank=True, null=True)
    dominated_technologies = models.CharField(max_length=191, blank=True, null=True)
    tec_basic_knowledge = models.CharField(max_length=191, blank=True, null=True)
    school_name = models.CharField(max_length=150, blank=True, null=True)
    linkedin = models.CharField(max_length=191, blank=True, null=True)
    cv = models.CharField(max_length=191, blank=True, null=True)
    github = models.CharField(max_length=191, blank=True, null=True)
    skype = models.CharField(max_length=191, blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)
    education = models.PositiveIntegerField()
    salary = models.PositiveIntegerField()
    change_address = models.PositiveIntegerField(blank=True, null=True)
    wepow_id = models.CharField(max_length=255, blank=True, null=True)
    credits = models.IntegerField(blank=True, null=True)
    points = models.IntegerField(blank=True, null=True)
    jira_user = models.CharField(max_length=45, blank=True, null=True)
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING, blank=True, null=True)
    university_id = models.PositiveIntegerField(blank=True, null=True)
    admission_date = models.DateField(blank=True, null=True)
    city_0 = models.ForeignKey(City, models.DO_NOTHING, db_column='city_id', blank=True, null=True)  # Field renamed because of name conflict.
    inn = models.IntegerField()
    onboarding = models.IntegerField(blank=True, null=True)
    previous_work = models.CharField(max_length=120, blank=True, null=True)
    phone = models.CharField(max_length=45, blank=True, null=True)
    visibility = models.IntegerField()
    stripe_customer = models.CharField(max_length=100, blank=True, null=True)
    gender = models.CharField(max_length=6, blank=True, null=True)
    country_id = models.PositiveIntegerField(blank=True, null=True)
    introduction = models.IntegerField()
    platform_update = models.IntegerField()
    enabled = models.IntegerField()
    technology = models.CharField(max_length=45, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'talent'


class TalentAnswer(models.Model):
    talent_evaluation_answer = models.ForeignKey('TalentEvaluationAnswer', models.DO_NOTHING)
    answer = models.ForeignKey(Answer, models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'talent_answer'


class TalentBadges(models.Model):
    talent_id = models.PositiveIntegerField()
    badge = models.CharField(max_length=255)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'talent_badges'


class TalentCategory(models.Model):
    talent = models.ForeignKey(Talent, models.DO_NOTHING)
    category = models.ForeignKey(Category, models.DO_NOTHING)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'talent_category'


class TalentChallenge(models.Model):
    talent = models.ForeignKey(Talent, models.DO_NOTHING)
    challenge = models.ForeignKey(Challenge, models.DO_NOTHING)
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING, blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    weight = models.FloatField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'talent_challenge'


class TalentChallengeCompetence(models.Model):
    talent = models.ForeignKey(Talent, models.DO_NOTHING)
    challenge_competence = models.ForeignKey(ChallengeCompetence, models.DO_NOTHING)
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    url = models.CharField(max_length=150, blank=True, null=True)
    path_file = models.CharField(max_length=150, blank=True, null=True)
    challenge_level = models.ForeignKey(ChallengeLevel, models.DO_NOTHING, blank=True, null=True)
    weight = models.FloatField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'talent_challenge_competence'


class TalentChallengeCompetenceFeedback(models.Model):
    feedback = models.ForeignKey(Feedback, models.DO_NOTHING)
    talent_challenge_competence = models.ForeignKey(TalentChallengeCompetence, models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'talent_challenge_competence_feedback'


class TalentChallengeCompetenceTraining(models.Model):
    talent = models.ForeignKey(Talent, models.DO_NOTHING)
    challenge = models.ForeignKey(Challenge, models.DO_NOTHING)
    competence = models.ForeignKey(Competence, models.DO_NOTHING)
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING, blank=True, null=True)
    resource = models.ForeignKey(Resource, models.DO_NOTHING)
    viewed = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'talent_challenge_competence_training'


class TalentChallengeEvaluator(models.Model):
    talent_challenge_competence = models.ForeignKey(TalentChallengeCompetence, models.DO_NOTHING)
    evaluator = models.ForeignKey(Evaluator, models.DO_NOTHING)
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING, blank=True, null=True)
    status = models.CharField(max_length=8)
    assignment_date = models.DateTimeField(blank=True, null=True)
    expiration_date = models.DateTimeField(blank=True, null=True)
    weight = models.FloatField()
    talent_feedback_weight = models.FloatField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'talent_challenge_evaluator'


class TalentChallengeEvaluatorRubric(models.Model):
    talent_challenge_evaluator = models.ForeignKey(TalentChallengeEvaluator, models.DO_NOTHING)
    challenge_rubric = models.ForeignKey(ChallengeRubric, models.DO_NOTHING)
    weight = models.FloatField()
    description = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'talent_challenge_evaluator_rubric'


class TalentCheckList(models.Model):
    talent = models.ForeignKey(Talent, models.DO_NOTHING)
    check_list = models.ForeignKey(CheckList, models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'talent_check_list'


class TalentCompetence(models.Model):
    talent = models.ForeignKey(Talent, models.DO_NOTHING)
    competence = models.ForeignKey(Competence, models.DO_NOTHING)
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING, blank=True, null=True)
    weight = models.FloatField()
    question_level = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'talent_competence'


class TalentCompetenceTraceability(models.Model):
    talent = models.ForeignKey(Talent, models.DO_NOTHING)
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING, blank=True, null=True)
    competence = models.ForeignKey(Competence, models.DO_NOTHING)
    weight = models.FloatField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'talent_competence_traceability'


class TalentCourse(models.Model):
    talent = models.ForeignKey(Talent, models.DO_NOTHING)
    course = models.ForeignKey(Course, models.DO_NOTHING)
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING, blank=True, null=True)
    points = models.IntegerField()
    credits = models.IntegerField()
    qualification = models.FloatField()
    level = models.CharField(max_length=13)
    file = models.CharField(max_length=160, blank=True, null=True)
    status = models.CharField(max_length=10)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)
    acreditation_date = models.DateField(blank=True, null=True)
    last_ranking = models.IntegerField()
    softskill_report = models.TextField(blank=True, null=True)
    english_report = models.TextField(blank=True, null=True)
    acquired_competences = models.IntegerField()
    base10_competence = models.FloatField(blank=True, null=True)
    level_competence = models.CharField(max_length=13)

    class Meta:
        managed = False
        db_table = 'talent_course'


class TalentCourseCompetence(models.Model):
    talent = models.ForeignKey(Talent, models.DO_NOTHING)
    course_competence = models.ForeignKey(CourseCompetence, models.DO_NOTHING)
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING, blank=True, null=True)
    status = models.IntegerField()
    weight = models.FloatField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'talent_course_competence'


class TalentCourseEvaluation(models.Model):
    talent = models.ForeignKey(Talent, models.DO_NOTHING)
    course = models.ForeignKey(Course, models.DO_NOTHING)
    evaluation = models.ForeignKey(Evaluation, models.DO_NOTHING)
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING, blank=True, null=True)
    attempts = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'talent_course_evaluation'


class TalentCourseFile(models.Model):
    talent_course = models.ForeignKey(TalentCourse, models.DO_NOTHING)
    name = models.CharField(max_length=50)
    file = models.CharField(max_length=160)
    is_system = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'talent_course_file'


class TalentCourseResource(models.Model):
    talent_course = models.ForeignKey(TalentCourse, models.DO_NOTHING)
    resource = models.ForeignKey(Resource, models.DO_NOTHING)
    flag = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'talent_course_resource'


class TalentEnterprise(models.Model):
    talent = models.ForeignKey(Talent, models.DO_NOTHING)
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'talent_enterprise'


class TalentEvaluation(models.Model):
    talent = models.ForeignKey(Talent, models.DO_NOTHING)
    evaluation = models.ForeignKey(Evaluation, models.DO_NOTHING)
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING, blank=True, null=True)
    status = models.IntegerField()
    weight = models.FloatField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)
    is_old = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'talent_evaluation'


class TalentEvaluationAnswer(models.Model):
    talent = models.ForeignKey(Talent, models.DO_NOTHING)
    talent_evaluation_competence = models.ForeignKey('TalentEvaluationCompetence', models.DO_NOTHING)
    question = models.TextField()
    talent_answer = models.TextField(blank=True, null=True)
    correct_answer = models.TextField()
    points = models.FloatField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)
    question_0 = models.ForeignKey(Question, models.DO_NOTHING, db_column='question_id')  # Field renamed because of name conflict.
    talent_answer_0 = models.ForeignKey(Answer, models.DO_NOTHING, db_column='talent_answer_id', blank=True, null=True)  # Field renamed because of name conflict.
    pending = models.IntegerField()
    question_category = models.ForeignKey(QuestionCategory, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'talent_evaluation_answer'


class TalentEvaluationCompetence(models.Model):
    talent = models.ForeignKey(Talent, models.DO_NOTHING)
    evaluation_competence = models.ForeignKey(EvaluationCompetence, models.DO_NOTHING)
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING, blank=True, null=True)
    level = models.ForeignKey(Level, models.DO_NOTHING, blank=True, null=True)
    weight = models.FloatField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)
    extra = models.IntegerField()
    vocational_report = models.TextField(blank=True, null=True)
    data_16fp = models.TextField(blank=True, null=True)
    data_reddin = models.TextField(blank=True, null=True)
    data_wonderlic = models.TextField(blank=True, null=True)
    data_cleaver = models.TextField(blank=True, null=True)
    data_english = models.TextField(blank=True, null=True)
    can_continue = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'talent_evaluation_competence'


class TalentEvaluationCompetenceExcludedCompetence(models.Model):
    talent_evaluation_competence_id = models.PositiveIntegerField()
    competence_id = models.PositiveIntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'talent_evaluation_competence_excluded_competence'


class TalentEvaluationCompetenceRecomendation(models.Model):
    talent_evaluation_competence = models.ForeignKey(TalentEvaluationCompetence, models.DO_NOTHING)
    question = models.ForeignKey(Question, models.DO_NOTHING)
    isactual = models.IntegerField(blank=True, null=True)
    iscorrect = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'talent_evaluation_competence_recomendation'


class TalentEvaluationCompetenceRecomendationFeedback(models.Model):
    talent_evaluation_competence_recomendation = models.ForeignKey(TalentEvaluationCompetenceRecomendation, models.DO_NOTHING)
    feedback = models.ForeignKey(Feedback, models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'talent_evaluation_competence_recomendation_feedback'


class TalentEvaluationCompetenceRecomendationFeedbackResource(models.Model):
    talent_evaluation_competence_recomendation_feedback = models.ForeignKey(TalentEvaluationCompetenceRecomendationFeedback, models.DO_NOTHING)
    resource = models.ForeignKey(Resource, models.DO_NOTHING)
    flag = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'talent_evaluation_competence_recomendation_feedback_resource'


class TalentGroup(models.Model):
    talent = models.ForeignKey(Talent, models.DO_NOTHING, blank=True, null=True)
    group = models.ForeignKey(Group, models.DO_NOTHING, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'talent_group'


class TalentGroups(models.Model):
    talent = models.ForeignKey(Talent, models.DO_NOTHING, blank=True, null=True)
    group_id = models.PositiveIntegerField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'talent_groups'


class TalentJob(models.Model):
    talent = models.ForeignKey(Talent, models.DO_NOTHING)
    job = models.ForeignKey(Job, models.DO_NOTHING)
    evaluator = models.ForeignKey(Evaluator, models.DO_NOTHING, blank=True, null=True)
    interview_date = models.DateTimeField(blank=True, null=True)
    interview_status = models.CharField(max_length=45)
    interview_video_url = models.CharField(max_length=200, blank=True, null=True)
    interview_challenge_url = models.CharField(max_length=200, blank=True, null=True)
    match = models.FloatField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'talent_job'


class TalentLanguages(models.Model):
    talent = models.ForeignKey(Talent, models.DO_NOTHING)
    languages = models.ForeignKey(Languages, models.DO_NOTHING)
    level = models.CharField(max_length=45, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'talent_languages'


class TalentLog(models.Model):
    talent = models.ForeignKey(Talent, models.DO_NOTHING)
    enterprise_id = models.PositiveIntegerField(blank=True, null=True)
    course = models.ForeignKey(Course, models.DO_NOTHING, blank=True, null=True)
    evaluation = models.ForeignKey(Evaluation, models.DO_NOTHING, blank=True, null=True)
    resource = models.ForeignKey(Resource, models.DO_NOTHING, blank=True, null=True)
    challenge = models.ForeignKey(Challenge, models.DO_NOTHING, blank=True, null=True)
    help_topic = models.ForeignKey(HelpTopic, models.DO_NOTHING, blank=True, null=True)
    action = models.CharField(max_length=11, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'talent_log'


class TalentOnboarding(models.Model):
    talent_id = models.PositiveIntegerField()
    onboarding_id = models.PositiveIntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'talent_onboarding'


class TalentOrientation(models.Model):
    talent_id = models.PositiveIntegerField()
    orientation_id = models.PositiveIntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'talent_orientation'


class TalentRecommendedContentViewed(models.Model):
    talent = models.ForeignKey(Talent, models.DO_NOTHING)
    resource = models.ForeignKey(Resource, models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'talent_recommended_content_viewed'


class TalentSchool(models.Model):
    talent_id = models.IntegerField()
    school_name = models.CharField(max_length=45, blank=True, null=True)
    school_type = models.CharField(max_length=45, blank=True, null=True)
    grade = models.CharField(max_length=45, blank=True, null=True)
    group = models.CharField(max_length=45, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'talent_school'


class TalentSkill(models.Model):
    talent = models.ForeignKey(Talent, models.DO_NOTHING)
    skill = models.ForeignKey(Skill, models.DO_NOTHING)
    verified = models.IntegerField()
    experience = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)
    weight = models.FloatField()
    of_interest = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'talent_skill'


class TalentSkillAlternative(models.Model):
    talent_id = models.PositiveIntegerField()
    name = models.CharField(max_length=45)
    of_interest = models.IntegerField()
    weight = models.FloatField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'talent_skill_alternative'


class TalentSocialNetwork(models.Model):
    talent = models.ForeignKey(Talent, models.DO_NOTHING)
    social_network = models.ForeignKey(SocialNetwork, models.DO_NOTHING)
    url = models.CharField(max_length=100)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'talent_social_network'


class TalentSpecialization(models.Model):
    talent = models.ForeignKey(Talent, models.DO_NOTHING)
    specialization = models.ForeignKey(Specialization, models.DO_NOTHING)
    level = models.CharField(max_length=50)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'talent_specialization'


class TalentSpecialties(models.Model):
    talent_id = models.IntegerField()
    specialty_id = models.IntegerField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'talent_specialties'


class TalentStudy(models.Model):
    talent = models.ForeignKey(Talent, models.DO_NOTHING)
    school_name = models.CharField(max_length=100)
    degree = models.CharField(max_length=100)
    grade = models.CharField(max_length=100, blank=True, null=True)
    status = models.CharField(max_length=45, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'talent_study'


class TalentSuggestedToEnterprise(models.Model):
    talent_id = models.PositiveIntegerField()
    enterprise_id = models.PositiveIntegerField()
    executive_id = models.PositiveIntegerField(blank=True, null=True)
    evaluator_id = models.PositiveIntegerField(blank=True, null=True)
    reserved = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'talent_suggested_to_enterprise'


class TalentTopic(models.Model):
    talent = models.ForeignKey(Talent, models.DO_NOTHING)
    topic = models.ForeignKey('Topic', models.DO_NOTHING)
    weight = models.FloatField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'talent_topic'


class TalentTopicCompetence(models.Model):
    talent = models.ForeignKey(Talent, models.DO_NOTHING)
    topic_competence = models.ForeignKey('TopicCompetence', models.DO_NOTHING)
    weight = models.FloatField()

    class Meta:
        managed = False
        db_table = 'talent_topic_competence'


class TalentUdemyCourseActivity(models.Model):
    talent = models.ForeignKey(Talent, models.DO_NOTHING)
    id_udemy_course = models.IntegerField(blank=True, null=True)
    title = models.CharField(max_length=150, blank=True, null=True)
    user_role = models.CharField(max_length=45, blank=True, null=True)
    user_is_deactivated = models.IntegerField(blank=True, null=True)
    course_duration = models.FloatField(blank=True, null=True)
    completion_ratio = models.FloatField(blank=True, null=True)
    num_video_consumed_minutes = models.FloatField(blank=True, null=True)
    course_enroll_date = models.DateTimeField(blank=True, null=True)
    course_start_date = models.DateTimeField(blank=True, null=True)
    course_completion_date = models.DateTimeField(blank=True, null=True)
    first_completion_date = models.DateTimeField(blank=True, null=True)
    last_accessed_date = models.DateTimeField(blank=True, null=True)
    categories = models.CharField(max_length=100, blank=True, null=True)
    url = models.CharField(max_length=200, blank=True, null=True)
    image = models.CharField(max_length=200, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'talent_udemy_course_activity'


class TalentUdemyCourseViewed(models.Model):
    talent = models.ForeignKey(Talent, models.DO_NOTHING)
    udemy_course_id = models.CharField(max_length=45)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'talent_udemy_course_viewed'


class TalentWork(models.Model):
    talent_id = models.PositiveIntegerField(blank=True, null=True)
    company_name = models.CharField(max_length=45)
    position = models.CharField(max_length=45)
    description = models.TextField(blank=True, null=True)
    current = models.IntegerField()
    start_date = models.DateField(blank=True, null=True)
    end_date = models.DateField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'talent_work'


class TemporalAccess(models.Model):
    talent = models.ForeignKey(Talent, models.DO_NOTHING)
    udemy_project = models.ForeignKey('UdemyProjects', models.DO_NOTHING)
    expiration_date = models.DateTimeField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'temporal_access'


class Topic(models.Model):
    name = models.CharField(max_length=45)
    description = models.TextField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'topic'


class TopicCompetence(models.Model):
    competence = models.ForeignKey(Competence, models.DO_NOTHING)
    topic = models.ForeignKey(Topic, models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'topic_competence'


class TrainingMethodology(models.Model):
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING)
    name = models.CharField(max_length=45)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'training_methodology'


class Trait(models.Model):
    short_name = models.CharField(max_length=100)
    name = models.CharField(max_length=200)
    characteristic = models.TextField()
    lower_rank = models.CharField(max_length=45, blank=True, null=True)
    top_rank = models.CharField(max_length=45, blank=True, null=True)
    type = models.CharField(max_length=9)
    display_images = models.CharField(max_length=6)
    display_feedbacks = models.CharField(max_length=6)
    competence = models.ForeignKey(Competence, models.DO_NOTHING)
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING, blank=True, null=True)
    admin = models.ForeignKey(Admin, models.DO_NOTHING, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'trait'


class TraitFeedback(models.Model):
    trait = models.ForeignKey(Trait, models.DO_NOTHING)
    feedback = models.ForeignKey(Feedback, models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'trait_feedback'


class TraitImage(models.Model):
    trait = models.ForeignKey(Trait, models.DO_NOTHING)
    image = models.ForeignKey(Image, models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'trait_image'


class TraitRule(models.Model):
    trait = models.ForeignKey(Trait, models.DO_NOTHING)
    factor_1 = models.CharField(max_length=45, blank=True, null=True)
    is_high_1 = models.IntegerField(blank=True, null=True)
    factor_2 = models.CharField(max_length=45, blank=True, null=True)
    is_high_2 = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    value = models.CharField(max_length=45)

    class Meta:
        managed = False
        db_table = 'trait_rule'


class Transactions(models.Model):
    talent = models.ForeignKey(Talent, models.DO_NOTHING, blank=True, null=True)
    credit = models.ForeignKey(Credits, models.DO_NOTHING, blank=True, null=True)
    point = models.ForeignKey(Points, models.DO_NOTHING, blank=True, null=True)
    issue_req = models.ForeignKey(IssueReqs, models.DO_NOTHING, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'transactions'


class Translate(models.Model):
    code = models.CharField(max_length=45)
    translate = models.CharField(max_length=45)

    class Meta:
        managed = False
        db_table = 'translate'


class Udemy(models.Model):
    last_name = models.CharField(max_length=100)
    user = models.ForeignKey('Users', models.DO_NOTHING, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'udemy'


class UdemyCategories(models.Model):
    name = models.CharField(max_length=200, blank=True, null=True)
    group = models.CharField(max_length=45, blank=True, null=True)
    level = models.IntegerField(blank=True, null=True)
    level_name = models.CharField(max_length=45, blank=True, null=True)
    school_type = models.CharField(max_length=45, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'udemy_categories'


class UdemyCourse(models.Model):
    udemy_id = models.CharField(max_length=45)
    date_added = models.CharField(max_length=45)
    language = models.CharField(max_length=45)
    name = models.CharField(max_length=250)
    category = models.CharField(max_length=45)
    subcategory = models.CharField(max_length=45)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'udemy_course'


class UdemyProjects(models.Model):
    prefix = models.CharField(max_length=45, blank=True, null=True)
    host = models.CharField(max_length=100, blank=True, null=True)
    account_id = models.CharField(max_length=45, blank=True, null=True)
    client_id = models.CharField(max_length=200, blank=True, null=True)
    client_secret = models.CharField(max_length=300, blank=True, null=True)
    enabled = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'udemy_projects'


class UdemyResources(models.Model):
    id_udemy_course = models.CharField(max_length=45, blank=True, null=True)
    title = models.CharField(max_length=200, blank=True, null=True)
    description = models.CharField(max_length=8000, blank=True, null=True)
    url = models.CharField(max_length=200, blank=True, null=True)
    categories = models.CharField(max_length=200, blank=True, null=True)
    instructors = models.CharField(max_length=200, blank=True, null=True)
    image = models.CharField(max_length=200, blank=True, null=True)
    locale = models.CharField(max_length=45, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'udemy_resources'


class Unenrollers(models.Model):
    group_id = models.CharField(max_length=45, blank=True, null=True)
    user_id = models.CharField(max_length=45, blank=True, null=True)
    member_id = models.CharField(max_length=45, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'unenrollers'


class Universities(models.Model):
    user = models.ForeignKey('Users', models.DO_NOTHING)
    type = models.CharField(max_length=30, blank=True, null=True)
    max_talents = models.IntegerField(blank=True, null=True)
    personal_name = models.CharField(max_length=255, blank=True, null=True)
    lastname = models.CharField(max_length=255, blank=True, null=True)
    summary = models.TextField(blank=True, null=True)
    address = models.CharField(max_length=255, blank=True, null=True)
    state = models.CharField(max_length=45, blank=True, null=True)
    city = models.CharField(max_length=45, blank=True, null=True)
    country = models.CharField(max_length=45, blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    udemy_project = models.ForeignKey(UdemyProjects, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'universities'


class UserPlatformSettings(models.Model):
    user = models.ForeignKey('Users', models.DO_NOTHING)
    logo = models.CharField(max_length=215)
    primary = models.CharField(max_length=7)
    secondary = models.CharField(max_length=7)
    background = models.CharField(max_length=7)
    font = models.CharField(max_length=7)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'user_platform_settings'


class UserTokenProfile(models.Model):
    user = models.ForeignKey('Users', models.DO_NOTHING)
    token_id = models.CharField(max_length=100)
    scope = models.CharField(max_length=45)
    profile_id = models.PositiveIntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'user_token_profile'


class Users(models.Model):
    name = models.CharField(max_length=191)
    email = models.CharField(unique=True, max_length=191)
    password = models.CharField(max_length=191)
    type_user = models.IntegerField(blank=True, null=True)
    remember_token = models.CharField(max_length=100, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    img = models.CharField(max_length=255, blank=True, null=True)
    facebook_id = models.CharField(max_length=255, blank=True, null=True)
    linkedin_id = models.CharField(max_length=255, blank=True, null=True)
    verify = models.IntegerField()
    verify_token = models.CharField(max_length=255, blank=True, null=True)
    recover_password = models.CharField(max_length=255, blank=True, null=True)
    wepowapikey = models.CharField(db_column='wepowApikey', max_length=255, blank=True, null=True)  # Field name made lowercase.
    temporal_password = models.IntegerField()
    terms_privacy = models.IntegerField()
    deleted_at = models.DateTimeField(blank=True, null=True)
    lang = models.CharField(max_length=45)
    default_scope = models.CharField(max_length=45, blank=True, null=True)
    default_profile_id = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'users'


class Variable(models.Model):
    type = models.CharField(max_length=45, db_collation='utf8mb4_0900_ai_ci')
    name = models.CharField(max_length=45, db_collation='utf8mb4_0900_ai_ci')
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'variable'


class ViewedBy(models.Model):
    talent_id = models.PositiveIntegerField()
    enterprise = models.ForeignKey(Enterprise, models.DO_NOTHING, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'viewed_by'


class VisibleFor(models.Model):
    course = models.ForeignKey(Course, models.DO_NOTHING)
    talent = models.ForeignKey(Talent, models.DO_NOTHING)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'visible_for'


class WaitingLists(models.Model):
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    name = models.CharField(max_length=45)
    email = models.CharField(max_length=45)
    phone = models.CharField(max_length=45)
    program_name = models.CharField(max_length=45)
    program_code = models.CharField(max_length=45)

    class Meta:
        managed = False
        db_table = 'waiting_lists'


class WepowInterviews(models.Model):
    wepow_interview = models.CharField(max_length=255)
    wepow_candidate = models.CharField(max_length=255)
    wepow_interview_candidate = models.CharField(max_length=255)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    talent_id = models.IntegerField(blank=True, null=True)
    enterprise_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'wepow_interviews'


class Widget(models.Model):
    context = models.ForeignKey(Context, models.DO_NOTHING)
    code = models.CharField(max_length=45)
    name = models.CharField(max_length=45)
    description = models.CharField(max_length=45, blank=True, null=True)
    source_type = models.CharField(max_length=45)
    group_by = models.CharField(max_length=45)
    config = models.TextField(blank=True, null=True)
    icon = models.CharField(max_length=45, blank=True, null=True)
    icon_color = models.CharField(max_length=10, blank=True, null=True)
    variant = models.CharField(max_length=45, blank=True, null=True)
    order_resume = models.IntegerField()
    visible = models.IntegerField()
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'widget'
