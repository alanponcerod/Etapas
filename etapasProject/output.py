# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Users(models.Model):
    name = models.CharField(max_length=191)
    email = models.CharField(unique=True, max_length=191)
    password = models.CharField(max_length=191)
    type_user = models.IntegerField(blank=True, null=True)
    remember_token = models.CharField(max_length=100, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    img = models.CharField(max_length=255, blank=True, null=True)
    facebook_id = models.CharField(max_length=255, blank=True, null=True)
    linkedin_id = models.CharField(max_length=255, blank=True, null=True)
    verify = models.IntegerField()
    verify_token = models.CharField(max_length=255, blank=True, null=True)
    recover_password = models.CharField(max_length=255, blank=True, null=True)
    wepowapikey = models.CharField(db_column='wepowApikey', max_length=255, blank=True, null=True)  # Field name made lowercase.
    temporal_password = models.IntegerField()
    terms_privacy = models.IntegerField()
    deleted_at = models.DateTimeField(blank=True, null=True)
    lang = models.CharField(max_length=45)
    default_scope = models.CharField(max_length=45, blank=True, null=True)
    default_profile_id = models.PositiveIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'users'
